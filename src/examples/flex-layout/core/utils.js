function arrayWrap(obj) {
    if(obj == null || obj == undefined || typeof obj.length != 'number') {
        return [obj];
    }
    return obj;
}

function filterObj(props, blacklist) {
    const copied = {...props};
    for(const prop of blacklist) {
        delete copied[prop];
    }
    return copied;
}

function isNodeWebkit() {
    return window.require != undefined;
}

export { arrayWrap, filterObj, isNodeWebkit };