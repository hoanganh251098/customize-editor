import { makeAutoObservable, observe, extendObservable } from "mobx";
import { action, observable } from "mobx";
import * as core from '../core/*.js';

const uuidv4 = core.uuidv4.default({useRegistry: false});

function createDeepObservable(obj) {
    if(obj == null || typeof obj != 'object') return obj;
    const clone = observable(obj);
    for(const k in obj) {
        clone[k] = createDeepObservable(clone[k]);
    }
    return clone;
}

const store = makeAutoObservable({
    mode: ['dev'],
    activeMenuItemID: null,
    dragIndicator: {
        id: null,
        element: null,
        type: null,
        mouseX: 0,
        mouseY: 0
    },
    dropData: {},
    selectedNodeID: null,
    activeTabIdx: null,
    tabs_v2: [],
    activeTabID_v2: null,
    hoveringTab: {
        tabID: null,
        side: null
    },
    menu: {
        children: [],
        show: true
    },
    assetRoot: null,
    assets: { children: [] },
    dialogs: [],
    plugins: [],
    devObj: {},
    document: {
        _id: 'document',
        children: []
    },
    getters: {
        getByID(resources, rID) {
            return resources.filter(resource => resource._id == rID)[0];
        },
        getIndexByID(resources, rID) {
            for(const [idx, resource] of resources.entries()) {
                if(resource._id == rID) {
                    return idx;
                }
            }
            return null;
        },
        isInMode(modeName) {
            return store.mode.includes(modeName);
        },
        tab(tabID) {
            return this.getByID(store.tabs_v2, tabID);
        },
        selectedNode() {
            let result;
            const dfs = (node) => {
                if(result) return;
                if(node._id == store.selectedNodeID) result = node;
                for(const childNode of node.children ?? []) {
                    dfs(childNode);
                }
            }
            dfs(store.document);
            return result;
        }
    }
});

const actions = {
    updateTestValue(v) {
        store.getters.__test = v;
    },
    enableMode(modeName) {
        if(!store.mode.includes(modeName)) {
            store.mode.push(modeName);
        }
    },
    disableMode(modeName) {
        const modeIndex = store.mode.indexOf(modeName);
        if(modeIndex > -1) {
            store.mode.splice(modeIndex, 1);
        }
    },
    getResource(resources, rID) {
        return resources.filter(resource => resource._id == rID)[0];
    },
    setActiveMenuItem(itemID) {
        store.activeMenuItemID = itemID;
    },
    setSelectedNode(nodeID) {
        store.selectedNodeID = nodeID;
        events.emit('setSelectedNode', nodeID);
    },
    setDragIndicator(data) {
        Object.assign(store.dragIndicator, data);
    },
    clearDragIndicator() {
        core.utils.clearObject(store.dragIndicator);
        Object.assign(store.dragIndicator, {
            id: null,
            element: null,
            type: null,
            mouseX: 0,
            mouseY: 0
        });
    },
    setDropData(data) {
        Object.assign(store.dropData, data);
    },
    clearDropData(data) {
        core.utils.clearObject(store.dropData);
    },
    addTab(tabData) {
        const tabID = uuidv4();
        store.tabs_v2.push({
            _id: tabID,
            ...tabData
        });
        return tabID;
    },
    updateTab(tabID, tabData) {
        const tab = store.getters.getByID(store.tabs_v2, tabID);
        tab && Object.assign(tab, tabData);
    },
    removeTab(tabID) {
        const tabIdx = store.getters.getIndexByID(store.tabs_v2, tabID);
        store.tabs_v2.splice(tabIdx, 1);
    },
    setActiveTab(tabID) {
        store.activeTabID_v2 = tabID;
    },
    setHoveringTab(data) {
        Object.assign(store.hoveringTab, data);
    },
    clearHoveringTab() {
        core.utils.clearObject(store.hoveringTab);
        Object.assign(store.hoveringTab, {
            tabID: null,
            side: null
        });
    },
    moveDraggingTab() {
        if(!store.hoveringTab.tabID) return;
        const clonedTabs = store.tabs_v2.slice();
        const activeTabData = store.getters.getByID(store.tabs_v2, store.activeTabID_v2);
        const draggingTabIdx = store.getters.getIndexByID(store.tabs_v2, store.activeTabID_v2);
        store.tabs_v2.splice(draggingTabIdx, 1);
        const hoveringTabIdx = store.getters.getIndexByID(store.tabs_v2, store.hoveringTab.tabID);
        const dropIdx = store.hoveringTab.side == 'left' ? hoveringTabIdx : hoveringTabIdx + 1;
        store.tabs_v2.splice(dropIdx, 0, activeTabData);
    },
    openDialog(config) {
        const {title, body, init} = config;
        const dialogID = uuidv4();
        const dialogData = {
            _id: dialogID,
            title: title,
            body: body
        };
        store.dialogs.push(dialogData);
        return dialogID;
    },
    closeDialog(dialogID) {
        const dialogIndex = store.getters.getIndexByID(store.dialogs, dialogID);
        store.dialogs.splice(dialogIndex, 1);
    },
    updateDialog(dialogID, dialogData) {
        const dialog = store.dialogs.filter(d => d._id == dialogID)[0];
        Object.assign(dialog, dialogData);
    },
    setAssetRoot(assetRoot) {
        store.assetRoot = assetRoot;
        events.emit('assetRootSet');
    },
    scanAssets(callback) {
        if(!store.assetRoot) return;
        const fs = nw.require('fs');
        const path = nw.require('path');
        if(!fs.existsSync(store.assetRoot)) return;
        const walk = function(dir, done) {
            let results = [];
            fs.readdir(dir, function(err, list) {
                if (err) return done(err);
                let pending = list.length;
                if (!pending) return done(null, results);
                list.forEach(function(file) {
                    file = path.resolve(dir, file);
                    fs.stat(file, function(err, stat) {
                        if (stat && stat.isDirectory()) {
                            walk(file, function(err, res) {
                                results = results.concat(res);
                                if (!--pending) done(null, results);
                            });
                        } else {
                            results.push(file);
                            if (!--pending) done(null, results);
                        }
                    });
                });
            });
        };
        const splitPath = function(rawPath) {
            const relativePath = path.relative(store.assetRoot, rawPath).replaceAll('\\', '/');
            return relativePath.split('/');
        };
        walk(store.assetRoot, (err, result) => {
            const assets = {
                children: [],
                show: true
            };
            for(const filePath of result.sort()) {
                const splitted = splitPath(filePath);
                let currentNode = assets;
                for(const [level, part] of splitted.entries()) {
                    const isFile = level == splitted.length - 1;
                    if(currentNode.children.filter(item => item.name == part && item.isFile == isFile).length == 0) {
                        const data = {
                            _id: uuidv4(),
                            isFile,
                            name: part
                        };
                        if(!isFile) {
                            data.children = [];
                            data.show = false;
                        }
                        currentNode.children.push(data);
                    }
                    currentNode = currentNode.children.filter(item => item.name == part && item.isFile == isFile)[0];
                }
            }
            Object.assign(store.assets, assets);
            callback && callback();
            events.emit('assetReady');
        });
    },
    toggleAssetNode(data) {
        data.show = !data.show;
    },
    devRegister(k, value) {
        store.devObj[k] = value;
    },
    registerPlugin(pluginName, plugin) {
        store.plugins.push({
            name: pluginName,
            instance: plugin
        });
    },
    createDocumentNode(data) {
        const nodeData = {
            _id: uuidv4(),
            type: data.type,
            title: data.title,
            extras: {},
            commonData: {
                left: {
                    label: 'Left',
                    type: 'number'
                },
                top: {
                    label: 'Top',
                    type: 'number'
                },
                width: {
                    label: 'Width',
                    type: 'number'
                },
                height: {
                    label: 'Height',
                    type: 'number'
                },
                rotation: {
                    label: 'Rotation',
                    type: 'number'
                },
                opacity: {
                    label: 'Opacity',
                    type: 'number',
                    defaultValue: 100,
                    minValue: 0,
                    maxValue: 100
                },
                visibility: {
                    label: 'Visibility',
                    type: 'checkbox',
                    displayValues: ['Visible', 'Hidden'],
                    defaultValue: true
                },
                keepRatio: {
                    label: 'Keep ratio',
                    type: 'checkbox',
                    displayValues: ['True', 'False'],
                    defaultValue: true
                }
            },
            data: JSON.parse(JSON.stringify(data.schema ?? null)),
            helpers: data.helpers ?? {}
        };
        return createDeepObservable(nodeData);
    }
};

(function() {
    const dfs = obj => {
        try {
            for(const k in actions) {
                if(typeof actions[k] == 'function') {
                    actions[k] = action(actions[k])
                }
                else {
                    dfs(actions[k])
                }
            }
        }
        catch(e) {
            console.error(e);
        }
    };
    dfs(actions);
})();

window.store = store;
window.actions = actions;

export { store, actions };

