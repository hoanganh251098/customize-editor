import React, { useRef } from 'react';
import { DraggableCore } from "react-draggable";
import * as core from '../core/*.js';

const separatorSpan = 10;
const separatorBg = 'bg-gray-300';

function AppFlexLayoutContainer(props) {
    return <div className="flex items-stretch h-full">{props.children}</div>
}

function AppFlexLayoutChild(props) {
    return <div {...props} className={`flex-1 ${props.className ?? ''}`}></div>
}

function AppFlexLayout(props) {
    const flexDirection = props.vertical ? 'flex-col' : 'flex-row';
    const children = core.utils.arrayWrap(props.children);
    const containerRef = useRef(null);
    const childrenRefs = Array(children.length).fill(useRef(null));
    let nrUnspecifieds = 0;
    let availableSpan = [100, ...children].reduce((acc, child) => {
        if((child.props.span ?? null) == null) nrUnspecifieds++;
        return acc - (child.props.span ?? 0);
    });
    const separatorsRefs = Array(children.length - 1).fill(useRef(null));
    const Separator = React.forwardRef(({idx=0}={}, ref) => {
        const style = props.vertical ? {height:`${separatorSpan}px`, minHeight:`${separatorSpan}px`} : {width:`${separatorSpan}px`, minWidth:`${separatorSpan}px`};
        return <div ref={ref} className="flex justify-center items-center" style={style}>
            <DraggableCore onDrag={(...args) => handleDrag(idx, ...args)}>
                <div className={`w-1/2 h-1/2 hover:${separatorBg} cursor-pointer`}></div>
            </DraggableCore>
        </div>
    });
    const dragSession = {
        spans: [],
        spansPx: []
    };
    function backupSpans() {
        const totalSpan = [0, ...childrenRefs].reduce((acc, child) => {
            return acc + (props.vertical ? child.clientHeight : child.clientWidth);
        });
        childrenRefs.forEach((child, idx) => {
            dragSession.spans[idx] = (props.vertical ? child.clientHeight : child.clientWidth) / totalSpan * 100 + '%';
        });
    }
    function restoreSpans() {
        for(const [idx, child] of childrenRefs.entries()) {
            child.style.flexBasis = dragSession.spans[idx];
        }
    }
    const handleDrag = (idx, e) => {
        const leftChild = childrenRefs[idx];
        const rightChild = childrenRefs[idx + 1];
        const separator = separatorsRefs[idx];
        const leftChildBRect = leftChild.getBoundingClientRect();
        const rightChildBRect = rightChild.getBoundingClientRect();
        const separatorBRect = separator.getBoundingClientRect();
        const leftChildSpan = props.vertical ? leftChildBRect.height : leftChildBRect.width;
        const rightChildSpan = props.vertical ? rightChildBRect.height : rightChildBRect.width;
        const separatorSpan = props.vertical ? separatorBRect.height : separatorBRect.width;
        const leftChildSpanPercent = +leftChild.style.flexBasis.replace('%', '');
        const rightChildSpanPercent = +rightChild.style.flexBasis.replace('%', '');
        const totalSpan = leftChildSpan + rightChildSpan + separatorSpan;
        const totalSpanPercent = (leftChildSpanPercent + rightChildSpanPercent)
            * (leftChildSpan + rightChildSpan + separatorSpan)
            / (leftChildSpan + rightChildSpan);
        const afterLeftChildSpan = (props.vertical
            ? e.pageY - leftChildBRect.top
            : e.pageX - leftChildBRect.left) - separatorSpan / 2;
        const afterLeftChildSpanPercent = (afterLeftChildSpan / totalSpan) * totalSpanPercent;
        const afterRightChildSpanPercent = leftChildSpanPercent + rightChildSpanPercent - afterLeftChildSpanPercent;
        if(afterLeftChildSpanPercent < 0 || afterRightChildSpanPercent < 0) return;
        leftChild.style.flexBasis = `${afterLeftChildSpanPercent}%`;
        rightChild.style.flexBasis = `${afterRightChildSpanPercent}%`;
        backupSpans();
        restoreSpans();
    };
    const generatedChildren = [];
    children.forEach((child, idx) => {
        const flexBasis = child.props.span != undefined
            ? (+child.props.span) / 100 * 100
            : availableSpan / nrUnspecifieds / 100 * 100;
        generatedChildren.push(
            <div ref={(ref) => childrenRefs[idx] = ref} key={`${idx}`} className="flex items-stretch h-full" style={{flexBasis:`${flexBasis}%`}}>
                {child}
            </div>
        )
        if(idx < children.length - 1) {
            generatedChildren.push(
                <Separator ref={(ref) => separatorsRefs[idx] = ref} key={`separator-${idx}`} idx={idx}/>
            )
        }
    });
    return (
        <div ref={containerRef} className={`flex-1 flex ${flexDirection} items-stretch ${props.className ?? ''}`}>
            {generatedChildren}
        </div>
    )
}

export default {
    Container: AppFlexLayoutContainer,
    Layout: AppFlexLayout,
    Child: AppFlexLayoutChild
}
