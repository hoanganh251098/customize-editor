import { makeAutoObservable, observe } from "mobx";
window.observe = observe;
import FontFaceObserver from './lib/fontfaceobserver';
window.FontFaceObserver = FontFaceObserver;
import "./styles/layout.css";
import _ from "lodash";
import { EventEmitter, MultiPromise } from "./core/eventSystem";
import UUIDv4Generator from './core/uuidv4';
import React, { createContext, useReducer, useState, useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { action as registerAction } from "mobx";
import { observer } from "mobx-react";
import { DraggableCore } from "react-draggable";
import { Stage } from 'react-konva';
import Konva from 'konva';
import DockLayout, { isDragging } from 'rc-dock';
import { SketchPicker } from 'react-color';
import reactCSS from 'reactcss';

import {
    faPlus, faTimes, faSearch,
    faChevronDown, faChevronUp, faChevronRight,
    faSync, faEllipsisV, faTools, faCheck,
    faCaretRight, faCaretLeft,
    faFolder, faFolderOpen,
    faHandPaper } from "@fortawesome/free-solid-svg-icons";
import { faCircle, faSquare, faCheckSquare, faFile } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { store, actions } from './store';
import FlexLayout from './components/flexlayout';
import * as core from './core/*.js';
import $ from 'jquery';

const uuidv4 = UUIDv4Generator();
const events = new EventEmitter;
window.events = events;
events.register('showMenuItem');
events.register('assetRootSet');
events.register('assetReady');
events.register('requestRefreshCanvas');
events.register('stageReady');
events.register('canvasObjectSelected');
events.register('canvasObjectUnselected');
events.register('requestCanvasSelect');
events.register('requestCanvasUnselect');
events.register('requestLockObject');
events.register('canvasObjectDragStart');
events.register('canvasObjectDragEnd');
events.register('canvasObjectTransformStart');
events.register('canvasObjectTransformEnd');
events.register('requestSetCanvasScale');
events.register('requestCreateNewNode');

window.core = core;
window.handlers = {
    click: {}
};

class ColorPicker extends React.Component {
  state = {
    displayColorPicker: false,
    color: '',
  };

  constructor(props) {
    super(props);
    this.onChange = props.onChange;
    this.setState({
        ...this.state,
        color: props.value
    });
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChange = (color) => {
    const finalColor = color.hex + Math.round(color.rgb.a * 255).toString(16);
    this.setState({ color: finalColor });
    this.onChange && this.onChange({
        target: {
            value: finalColor
        }
    })
  };

  render() {
    const styles = reactCSS({
      'default': {
        color: {
          width: '100%',
          height: '14px',
          borderRadius: '2px',
          background: `${this.state.color || this.props.value}`,
        },
        swatch: {
          width: '100%',
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '20',
          right: '2rem'
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div className="w-full">
        <div style={ styles.swatch } onClick={ this.handleClick }>
          <div style={ styles.color } />
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClose }/>
          <SketchPicker color={ this.state.color || this.props.value } onChange={ this.handleChange } />
        </div> : null }

      </div>
    )
  }
}

function AppSearchInput(props) {
    let [state, setState] = useState({
        showClearBtn: false
    });
    const inputRef = useRef(null);
    const handleChange = (e) => {
        setState({
            showClearBtn: !!e.target.value.length
        });
    };
    const clearInput = () => {
        inputRef.current.value = '';
        setState({
            showClearBtn: false
        });
    };
    return (
        <div className="w-full relative">
            <input className="w-full h-full outline-none pl-3 pr-6 bg-gray-100 border focus:border-blue-400"
                ref={inputRef}
                placeholder={ props.placeholder ?? '' }
                spellCheck="false"
                autoComplete="off"
                autoCapitalize="off"
                autoCorrect="off"
                onChange={handleChange}
                ></input>
            <div className="flex justify-center items-center absolute top-0 right-0 w-2 h-full transform -translate-x-full">
                {state.showClearBtn && 
                    <button className="outline-none" onClick={clearInput}>
                        <FontAwesomeIcon icon={faTimes} className="opacity-50"/>
                    </button>
                }
                {!state.showClearBtn && 
                    <span>
                        <FontAwesomeIcon icon={faSearch} className="opacity-50 text-sm"/>
                    </span>
                }
            </div>
        </div>
    )
}

const AppInput = observer((props) => {
    return <input
        spellCheck="false"
        autoComplete="off"
        autoCapitalize="off"
        autoCorrect="off"
        {...props}
        ></input>
});

function AppCheckbox(props) {
    const verbose = props.verbose ?? ['True', 'False'];
    const [state, setState] = useState({
        value: props.value ?? false
    });
    const toggle = () => {
        const newValue = !state.value;
        props.onChange && props.onChange({
            target: {
                value: newValue
            }
        })
        setState({
            value: newValue
        });
    };
    return (
        <button onClick={toggle} className="flex">
            <span className="inline-block w-6 h-6 mr-2 bg-gray-400 text-white">
                {state.value && 
                    <FontAwesomeIcon icon={faCheck}/>
                }
            </span> {state.value ? verbose[0] : verbose[1]}
        </button>
    )
}

function AppTextarea(props) {
    return <textarea
        spellCheck="false"
        autoComplete="off"
        autoCapitalize="off"
        autoCorrect="off"
        {...props}
        ></textarea>
}

function AppButton(props) {
    return <button className="flex justify-center items-center px-2 border active:border-blue-400 bg-gray-100" {...props}>
        {props.children}
    </button>
}

function AppSelect(props) {
    const values = props.values ?? [];
    const [state, setState] = useState({
        selectedValue: props.value,
        show: false
    });
    const containerRef = useRef(null);
    const togglerRef = useRef(null);
    const selectedTitle = values.filter(v => v.value == state.selectedValue)[0]?.title ?? 'Select an option';
    const handleClick = (e) => {
        if(e.target == togglerRef.current || togglerRef.current.contains(e.target)) {
            setState({
                ...state,
                show: !state.show
            }); 
        }
    };
    const handleSelect = v => {
        const isChanged = v.value != state.selectedValue;
        setState({
            ...state,
            selectedValue: v.value,
            show: false,
        });
        isChanged && props.onChange && props.onChange({
            target: {
                value: v.value
            }
        });
    };
    return <div ref={containerRef} className="relative flex-1 w-full bg-gray-200" onClick={handleClick}>
        <div ref={togglerRef} className="relative w-full pl-2 px-6 bg-gray-200 outline-none truncate cursor-pointer">
            <span>{selectedTitle}</span>
            <FontAwesomeIcon icon={faChevronDown} className="absolute right-2 top-0 h-full text-xs opacity-50"/>
        </div>
        <div className={`absolute z-10 w-full p-2 bg-white ${state.show ? '' : 'hidden'}`}>
            { values.map(v => 
                <div className={`hover:bg-gray-100 ${v.value == state.selectedValue ? 'bg-gray-100' : ''}`} onClick={e => handleSelect(v)}>{v.title}</div>
            ) }
        </div>
    </div>
}

function AppTabIndicator(props) {
    let className = "flex items-center bg-gray-100 hover:bg-white py-1 px-4 border-l-2 border-l-gray-200 border-r-2 border-r-gray-200 "
    if(props.active)
        className="flex items-center bg-white py-1 px-4 border-l-2 border-l-transparent border-r-2 border-r-transparent border-b-2 border-b-yellow-300 "
    const clonedProps = {...props};
    delete clonedProps.active;

    return <div
            className={className + (clonedProps.className ?? '')}
            {...clonedProps}
        >
            {props.children}
        </div>
}

const AppProductNode = observer((props) => {
    const [state, setState] = useState({
        show: props.show ?? false
    });
    const nodeHandleRef = useRef(null);
    const nodeID = props.nodeID ?? uuidv4();
    const toggle = () => {
        setState({
            show: !state.show
        });
    };
    const handleStart = e => {
    };
    const handleDrag = e => {
        const brect = nodeHandleRef.current.getBoundingClientRect();
        if(!store.dragIndicator.element
            && !(e.pageX >= brect.left && e.pageX <= brect.right
            && e.pageY >= brect.top && e.pageY <= brect.bottom)) {
            actions.setDragIndicator({
                id: nodeID,
                element: <NodeHandle {...props}></NodeHandle>,
                type: 'ProductNode',
                mouseX: e.pageX,
                mouseY: e.pageY
            });
        }
        actions.setDragIndicator({
            mouseX: e.pageX,
            mouseY: e.pageY
        });
    };
    const handleStop = e => {
        actions.clearDragIndicator();
    };
    let mouseMoveVersion = 0;
    const handleMouseOver = e => {
        if(!store.dragIndicator.element) return;
        if(store.dragIndicator.type != 'ProductNode') return;
        if(store.dragIndicator.id == nodeID) return;
        const brect = nodeHandleRef.current.getBoundingClientRect();
        const isAtHead = e.pageX <= brect.left + brect.height;
        const isInUpperPart = e.pageY < brect.top + brect.height / 2;
        nodeHandleRef.current.classList.remove('border-blue-400');
        nodeHandleRef.current.classList.remove('border-t-blue-400');
        nodeHandleRef.current.classList.remove('border-b-blue-400');
        if(isAtHead) {
            nodeHandleRef.current.classList.add('border-blue-400');
        }
        else if(isInUpperPart) {
            nodeHandleRef.current.classList.add('border-t-blue-400');
        }
        else {
            nodeHandleRef.current.classList.add('border-b-blue-400');
        }
    };
    const handleMouseLeave = () => {
        nodeHandleRef.current.classList.remove('border-blue-400');
        nodeHandleRef.current.classList.remove('border-t-blue-400');
        nodeHandleRef.current.classList.remove('border-b-blue-400');
    };
    const handleClick = () => {
        actions.setSelectedNode(nodeID);
    };
    function NodeHandle(props) {
        const element = (
            <div className="absolute left-0 top-0 flex px-3 h-6 bg-white bg-opacity-90 pointer-events-none drag-indicator">
                <div className="flex justify-center items-center h-full w-6">
                    <FontAwesomeIcon icon={faCircle} className="text-xs opacity-50"/>
                </div>
                <span>{props.title ?? 'Untitled'}</span>
            </div>
        );
        return element;
    }
    return(
        <div>
            <div className="flex h-6">
                <div className="flex justify-center items-center h-4/5 w-6 min-w-6">
                    {
                        props.children &&
                        <button className="outline-none" onClick={toggle}>
                            <FontAwesomeIcon icon={state.show ? faChevronDown : faChevronRight} className="text-xs opacity-50"/>
                        </button>
                    }
                </div>
                <DraggableCore onStart={handleStart} onDrag={handleDrag} onStop={handleStop}>
                    <div ref={nodeHandleRef}
                        onClick={handleClick}
                        onDoubleClick={toggle}
                        className={`flex h-full w-full border border-transparent ${(store.selectedNodeID == nodeID) ? 'bg-gray-400 text-white' : 'hover:bg-gray-200'} handle`}
                        onMouseMove={handleMouseOver}
                        onMouseLeave={handleMouseLeave}
                    >
                        <div className="flex justify-center items-center h-full w-6 min-w-6">
                            <FontAwesomeIcon icon={faCircle} className="text-xs opacity-50"/>
                        </div>
                        <span>{props.title ?? 'Untitled'}</span>
                    </div>
                </DraggableCore>
            </div>
            <div className="pl-6">
                {state.show && props.children}
            </div>
        </div>
    )
});

const AppCanvasTabs = (() => {
    const AppTabIndicator2 = observer(props => {
        const nodeHandleRef = useRef(null);
        const data = props.data;
        const nodeID = data._id;
        const isActive = store.activeTabID_v2 == data._id;
        const isHovering = store.hoveringTab.tabID == data._id;
        let className = "flex items-center bg-gray-100 hover:bg-white py-1 px-4 border-l-2 border-l-gray-200 border-r-2 border-r-gray-200 text-black text-opacity-25 "
        if(isActive)
            className = "flex items-center bg-white py-1 px-4 \
            border-l-2 border-l-gray-200 border-r-2 border-r-gray-200 border-b-2 border-b-yellow-300 ";
        if(isHovering)
            className += store.hoveringTab.side == 'left' ? 'border-l-yellow-400' : 'border-r-yellow-400';
        const handleClick = e => {
            actions.setActiveTab(data._id);
        };
        const handleClose = async e => {
            await null;
            const tabIndex = store.getters.getIndexByID(store.tabs_v2, data._id);
            actions.removeTab(data._id);
            if(store.tabs_v2[tabIndex]) {
                actions.setActiveTab(store.tabs_v2[tabIndex]?._id);
            }
            else {
                actions.setActiveTab(store.tabs_v2[tabIndex-1]?._id);
            }
        };
        const handleStart = e => {
        };
        const handleDrag = async e => {
            if(!isActive) return;
            const brect = nodeHandleRef.current.getBoundingClientRect();
            if(!store.dragIndicator.element
                && !(e.pageX >= brect.left && e.pageX <= brect.right
                && e.pageY >= brect.top && e.pageY <= brect.bottom)) {
                actions.setDragIndicator({
                    id: nodeID,
                    element: <NodeHandle/>,
                    type: 'Tab',
                    mouseX: e.pageX,
                    mouseY: e.pageY
                });
            }
            actions.setDragIndicator({
                mouseX: e.pageX,
                mouseY: e.pageY
            });
        };
        const handleStop = e => {
            actions.moveDraggingTab();
            actions.clearDragIndicator();
            actions.clearHoveringTab();
        };
        const handleMouseOver = e => {
            if(!store.dragIndicator.element) return;
            if(store.dragIndicator.type != 'Tab') return;
            if(store.dragIndicator.id == nodeID) return;
            const brect = nodeHandleRef.current.getBoundingClientRect();
            const isInLeftPart = e.pageX <= brect.left + brect.width / 2;
            actions.setHoveringTab({
                tabID: nodeID,
                side: isInLeftPart ? 'left' : 'right'
            });
        };
        const handleMouseLeave = e => {
        };
        function NodeHandle() {
            return <div className="absolute left-0 top-0 flex bg-white bg-opacity-90 pointer-events-none drag-indicator">
                <div className={className}>
                    <span title={data.title} className="inline-block w-32 truncate">{data.title}</span>
                    { isActive
                        ?
                        <button className="outline-none inline-flex items-center w-2 h-full ml-2 opacity-50 hover:opacity-100">
                            <FontAwesomeIcon icon={faTimes} className="opacity-50"/>
                        </button>
                        : null
                    }
                </div>
            </div>
        }
        return <DraggableCore
                onStart={handleStart}
                onDrag={handleDrag}
                onStop={handleStop}
            >
                <div
                    ref={nodeHandleRef}
                    className={className} 
                    onClick={handleClick}
                    onMouseMove={handleMouseOver}
                    onMouseLeave={handleMouseLeave}
                >
                    <span title={data.title} className="inline-block w-32 truncate">{data.title}</span>
                    { isActive
                        ?
                        <button className="outline-none inline-flex items-center w-2 h-full ml-2 opacity-50 hover:opacity-100" onClick={handleClose}>
                            <FontAwesomeIcon icon={faTimes} className="opacity-50"/>
                        </button>
                        : null
                    }
                </div>
            </DraggableCore>
    });
    return observer(props => {
        const containerRef = useRef(null);
        const moveLeft = () => {
            containerRef.current.scrollBy({
                left: -100,
                behavior: 'smooth'
            });
        };
        const moveRight = () => {
            containerRef.current.scrollBy({
                left: 100,
                behavior: 'smooth'
            });
        };
        return <div className="relative w-full h-8 transition border-transparent">
            <div ref={containerRef} className="absolute z-0 w-full overflow-hidden inset-0 flex px-8">
                {
                    store.tabs_v2.length
                    ? store.tabs_v2.map(tabConfig => {
                        return <AppTabIndicator2 key={tabConfig._id} data={tabConfig}/>
                    })
                    : <AppTabIndicator2 key={'default-tab'} data={{_id: 'default-tab', title: '~default'}}/>
                }
            </div>
            <div className="relative w-full h-full flex justify-between pointer-events-none">
                <button
                    onClick={moveLeft}
                    className="outline-none inline-flex justify-center items-center w-8 h-full border-r-4 border-gray-200 hover:bg-white bg-gray-100 pointer-events-auto">
                    <FontAwesomeIcon icon={faCaretLeft} className="opacity-50"/>
                </button>
                <button
                    onClick={moveRight}
                    className="outline-none inline-flex justify-center items-center w-8 h-full border-l-4 border-gray-200 hover:bg-white bg-gray-100 pointer-events-auto">
                    <FontAwesomeIcon icon={faCaretRight} className="opacity-50"/>
                </button>
            </div>
        </div>
    })
})();

const AppMenuItem = observer(props => {
    const children = core.utils.arrayWrap(props.children);
    const childrenContainerRef = useRef(null);
    const id = props.dataID;
    const toggleChildren = () => {
        if(!store.activeMenuItemID?.includes(id)) {
            actions.setActiveMenuItem(id);
        }
        else {
            actions.setActiveMenuItem(null);
        }
    };
    const showChildren = () => {
        if(store.activeMenuItemID != id) {
            actions.setActiveMenuItem(id);
        }
    };
    const handleOuterMouseOver = () => {
        store.activeMenuItemID != null && showChildren()
    };
    const handleOpen = e => {
        if(!props.children && props.onClick) {
            actions.setActiveMenuItem(null);
            props.onClick();
        }
    };
    const childrenContainerEl = props.children && (
        <div ref={childrenContainerRef} className={`${store.activeMenuItemID?.includes(id) ? '' : 'hidden'} flex flex-col items-stretch absolute z-10 left-0 bottom-0 w-max transform translate-y-full bg-white border-l border-r border-b border-gray-300 px-1 py-1 mt-1 min-w-32`}>
            {  children.map((child, idx) => {
                return <child.type {...child.props} dataID={id + '-' + idx} key={id + '-' + idx} />;
            }) }
        </div>
    );
    if(props.outer != undefined) {
        return <div className="relative" data-id={id}>
            <span
                onClick={toggleChildren}
                onMouseOver={handleOuterMouseOver}
                className="inline-block w-full px-2 py-1 border-b border-transparent hover:border-blue-500">
                { props.title ?? 'MenuItem' }
            </span>
            <div className="absolute bottom-0 left-0">
                { childrenContainerEl }
            </div>
        </div>
    }
    return <div className="relative" data-id={id}>
        <span
            onMouseOver={showChildren}
            onClick={handleOpen}
            className="inline-block w-full px-2 bg-white hover:bg-blue-300 hover:text-white pr-6">
            { props.title ?? 'MenuItem' }
            { props.subtitle ? <span className="inline-block ml-2 opacity-50">{props.subtitle}</span> : null }
            { props.children && <FontAwesomeIcon icon={faCaretRight} className="text-xs absolute right-2 h-full"/>}
        </span>
        <div className="absolute -right-1 -top-1">
            { childrenContainerEl }
        </div>
    </div>
});

function AppNumberPropField(props) {}
function AppInputPropField(props) {}
function AppTextareaPropField(props) {}
function AppCheckboxPropField(props) {}

const AppDragIndicator = observer(props => {
    return store.dragIndicator.element;
});

(function() {
    observe(store.dragIndicator, change => {
        if(!store.dragIndicator.element) return;
        document.body.style.setProperty('--drag-x', store.dragIndicator.mouseX + 'px');
        document.body.style.setProperty('--drag-y', store.dragIndicator.mouseY + 'px');
    });
})();

const AppDialogs = observer(props => {
    return !store.dialogs.length ? null :
        <div className="flex justify-center items-center absolute top-0 left-0 w-full h-full bg-black bg-opacity-50">
            { store.dialogs.map((dialogConfig, idx) => {
                return <div className="absolute">
                    <div id={'dialog-'+dialogConfig._id} className="flex flex-col relative min-w-80 min-h-40 px-2 py-1 bg-gray-200 border" key={idx}
                    >
                        <div className="relative h-6">
                            <span className="font-semibold pr-6">{ dialogConfig.title }</span>
                            <div className="absolute right-1 top-0 h-full">
                                <button className="outline-none" onClick={e => actions.closeDialog(dialogConfig._id)}>
                                    <FontAwesomeIcon icon={faTimes} className="opacity-50"/>
                                </button>
                            </div>
                        </div>
                        { dialogConfig.body.$$typeof
                            ? <div className="flex-1 flex flex-col items-stretch">{dialogConfig.body}</div>
                            : <div className="flex-1 flex flex-col items-stretch" dangerouslySetInnerHTML={{ __html: dialogConfig.body }}></div>
                        }
                    </div>
                </div>
            }) }
        </div>
});

const AppRunningPlugins = observer(props => {
    if(!core.utils.isNodeWebkit()) return;
    return <AppMenuItem title="Running plugins" {...props}>
        {
            store.plugins.map(({name, instance}) => {
                return <AppMenuItem key={name} title={name.replace('.js', '')} onClick={e => instance.onShow && instance.onShow()}/>
            })
        }
    </AppMenuItem>
});

const AppDevMenu = observer(props => {
    return store.getters.isInMode('dev')
        ? <AppMenuItem title="DEV" dataID="dev" outer>
            { Object.entries(store.devObj).map(([k, v]) => {
                return <AppMenuItem
                    title={core.utils.parseCamel(k).map(word => word.toLowerCase()).join(' ')}
                    onClick={() => v()}
                    key={k}
                >
                </AppMenuItem>
            }) }
        </AppMenuItem>
        : null
        ;
});

const AppMenu = observer(props => {
    const menuTree = (menuItem, menuItemID) => {
        const isOuter = menuItem == undefined;
        if(isOuter) {
            menuItem = store.menu;
        }
        if(!menuItem.children) return;
        return menuItem.children.map((subItem, idx) => {
            if(isOuter) {
                const id = uuidv4();
                return <AppMenuItem title={subItem.title} dataID={id} outer key={idx}>
                    { menuTree(subItem, id) }
                </AppMenuItem>
            }
            return <AppMenuItem title={subItem.title} subtitle={subItem.subtitle} onClick={subItem.onClick} key={idx}>
                { menuTree(subItem, menuItemID + '-' + idx) }
            </AppMenuItem>
        })
    };
    return <>
        { menuTree() }
    </>
});

const AppAssetTree = observer(props => {
    const Node = ({level=0, data}={}) => {
        const key = (data.isFile ? 'file' : 'folder') + `-${level}-${data.name}`;
        return <div className="flex flex-col items-stretch w-full" key={key}>
            <div className="flex h-6">
                { Array(level).fill(null).map((_, idx) => <div className="pl-6" key={idx}></div>) }
                <div className="flex justify-center items-center min-w-8 w-8 h-6">
                    <button className="outline-none" onClick={e => !data.isFile && actions.toggleAssetNode(data)}>
                        { data.isFile
                            ? <FontAwesomeIcon icon={faFile} className="opacity-50"/>
                            : <FontAwesomeIcon icon={data.show ? faFolderOpen : faFolder} className="opacity-50"/>
                        }
                    </button>
                </div>
                <div className="flex-1 hover:bg-gray-200 whitespace-nowrap" onDoubleClick={e => !data.isFile && actions.toggleAssetNode(data)}>
                    <span>{data.name ?? 'local://'}</span>
                </div>
            </div>
            <div>
                { data.children && data.show
                    ?
                    data.children.map(child => Node({level: level + 1, data: child}))
                    : null
                }
            </div>
        </div>
    };

    return Node({data: store.assets})
});

const AppNodeProps = observer(props => {
    const AppInputForm = observer((p) => {
        const {key, valueConfig} = p.config;
        let inputForm = null;
        const updateValue = registerAction(e => {
            Object.assign(valueConfig, {
                value: e.target.value
            });
        });
        if(valueConfig.type == 'input') {
            inputForm = <div className="flex h-6">
                <div className="flex-1">
                    {valueConfig.label}
                </div>
                <div className="flex-1 bg-gray-200">
                    <AppInput type="text" className="w-full px-2 bg-gray-200 outline-none"
                        value={valueConfig.value ?? ''}
                        onChange={updateValue}
                    />
                </div>
            </div>;
        }
        else if(valueConfig.type == 'number') {
            inputForm = <div className="flex h-6">
                <div className="flex-1">
                    {valueConfig.label}
                </div>
                <div className="flex-1 bg-gray-200">
                    <AppInput type="number" className="w-full px-2 bg-gray-200 outline-none"
                        value={valueConfig.value ?? 0}
                        onChange={updateValue}
                    />
                </div>
            </div>;
        }
        else if(valueConfig.type == 'checkbox') {
            inputForm = <div className="flex h-6">
                <div className="flex-1">
                    {valueConfig.label}
                </div>
                <div className="flex-1">
                    <AppCheckbox
                        value={valueConfig.value ?? true}
                        verbose={valueConfig.displayValues}
                        onChange={updateValue}
                    ></AppCheckbox>
                </div>
            </div>;
        }
        else if(valueConfig.type == 'textarea') {
            inputForm = <div>
                <div>
                    {valueConfig.label}
                </div>
                <AppTextarea className="bg-gray-200 px-2 w-full outline-none" rows="5"
                    value={valueConfig.value ?? ''}
                    onChange={updateValue}
                ></AppTextarea>
            </div>;
        }
        else if(valueConfig.type == 'colorpicker') {
            inputForm = <div className="flex h-6">
                <div className="flex-1">
                    {valueConfig.label}
                </div>
                <div className="flex-1 bg-gray-200">
                    <ColorPicker
                        value={valueConfig.value ?? '#000000'}
                        onChange={updateValue}/>
                </div>
            </div>;
        }
        else if(valueConfig.type == 'select') {
            inputForm = <div className="flex h-6">
                <div className="flex-1">
                    {valueConfig.label}
                </div>
                <div className="flex-1 bg-gray-200">
                    <AppSelect
                        id={p.id}
                        value={valueConfig.value}
                        values={valueConfig.values}
                        onChange={updateValue}/>
                </div>
            </div>;
        }
        return inputForm;
    });
    return <>
        { store.selectedNodeID
            ? <>
            <div className="flex justify-center items-center h-6 bg-gray-300">
                <span>
                    <FontAwesomeIcon icon={faSquare}/> Box
                </span>
            </div>
            { Object.entries(store.getters.selectedNode()?.commonData ?? {}).map(([key, valueConfig], idx) => {
                return <React.Fragment key={idx}>
                    <div className="mt-1"></div>
                    <AppInputForm id={idx} config={{key, valueConfig}}/>
                </React.Fragment>
            }) }
            <div className="mt-1"></div>
            <div className="flex justify-center items-center h-6 bg-gray-300">
                <span>
                    <FontAwesomeIcon icon={faCircle}/> Node specific
                </span>
            </div>
            { Object.entries(store.getters.selectedNode()?.data ?? {}).map(([key, valueConfig], idx) => {
                return <React.Fragment key={idx}>
                    <div className="mt-1"></div>
                    <AppInputForm id={idx} config={{key, valueConfig}}/>
                </React.Fragment>
            }) }
            <div className="mt-1"></div>
            <div className="flex justify-center items-center h-6 bg-gray-300">
                <span>
                    <FontAwesomeIcon icon={faHandPaper}/> Helpers
                </span>
            </div>
            { Object.entries(store.getters.selectedNode()?.helpers ?? {}).map(([key, valueConfig], idx) => {
                return <React.Fragment key={idx}>
                    <div className="mt-1"></div>
                    <AppButton className="flex justify-center items-center px-2 border active:border-blue-400 bg-gray-200 w-full" onClick={e => valueConfig.onClick && valueConfig.onClick()}>
                        {valueConfig.title}
                    </AppButton>
                </React.Fragment>
            }) }
            </>
            : null
        }
    </>
});

const AppProductNodes = observer(props => {
    const Node = ({level=0, data}={}) => {
        return <AppProductNode title={level == 0 ? 'Document' : data.title} show={level == 0 ? 'true' : 'false'} nodeID={data._id} key={uuidv4()}>
            { data.children
                ?
                data.children.map(child => Node({level: level + 1, data: child}))
                : null
            }
        </AppProductNode>
    };
    return Node({data: store.document});
});

const AppPluginConfig = props => {
    const [state, setState] = useState({
        selectedPluginName: null,
    });

    const ActivePluginList = observer(props => {
        return <>
            {
                store.plugins.map(({name, instance}) => {
                    return <span
                        key={name}
                        className={`px-1 ${state.selectedPluginName == name ? 'bg-gray-200' : 'hover:bg-gray-100'}`}
                        onClick={e => setState({selectedPluginName: name})}
                    >{name.replace('.js', '')}</span>
                })
            }
        </>
    });

    const AvailablePluginList = observer(props => {
        return <>
            {
                core.pluginSystem.default.availablePlugins.filter(
                    pluginName => !store.plugins.map(({name, instance}) => name).includes(pluginName)
                ).map(name => {
                    return <span
                        key={name}
                        className={`px-1 ${state.selectedPluginName == name ? 'bg-gray-200' : 'hover:bg-gray-100'}`}
                        onClick={e => setState({selectedPluginName: name})}
                    >{name.replace('.js', '')}</span>
                })
            }
        </>
    });

    return <div className="flex-1 min-w-96 flex items-stretch py-1">
        <div className="w-2/3 flex flex-col">
            <span className="text-black text-opacity-50 text-sm">Enabled plugins</span>
            <div className="flex flex-col bg-white p-1">
                <ActivePluginList/>
            </div>
            <span className="text-black text-opacity-50 text-sm">Available plugins</span>
            <div className="flex flex-col bg-white p-1">
                <AvailablePluginList/>
            </div>
        </div>
        <div className="w-1/3 px-2 flex flex-col items-stretch">
            <span className="text-black text-opacity-0 text-sm">~</span>
            <AppButton>Enable</AppButton>
            <div className="mt-1"/>
            <AppButton>Disable</AppButton>
            <div className="mt-1"/>
            <AppButton>Move up</AppButton>
            <div className="mt-1"/>
            <AppButton>Move down</AppButton>
        </div>
    </div>
};

function App() {
    const canvasContainerRef = useRef(null);
    const [canvasSize, setCanvasSize] = useState({
        width: 0,
        height: 0
    });
    return (
        <div className="h-screen bg-gray-200 flex flex-col">
            <div className="bg-gray-200 h-8 flex justify-between">
                <div className="flex items-center px-2">
                    <AppMenu/>
                    <AppMenuItem title="Plugin" dataID="plugin" outer>
                        <AppRunningPlugins/>
                        <AppMenuItem title="Config"
                            onClick={e => {actions.openDialog({title: 'Plugin Config', body: <AppPluginConfig/>})}}></AppMenuItem>
                    </AppMenuItem>
                    <AppMenuItem title="Help" dataID="help" outer>
                        <AppMenuItem title="Check for updates"></AppMenuItem>
                        <AppMenuItem title="About"></AppMenuItem>
                    </AppMenuItem>
                    <AppDevMenu/>
                </div>
                <div className="flex-1" onClick={e => actions.setActiveMenuItem(null)}></div>
            </div>
            <div className="flex-1 p-2 flex items-stretch" onClick={e => actions.setActiveMenuItem(null)}>
                <FlexLayout.Layout>
                    <div span="20" className="flex-1 flex flex-col w-1/5 py-1">
                        <FlexLayout.Layout vertical={true}>
                            <div className="flex flex-col flex-1">
                                <div className="flex">
                                    <AppTabIndicator active={true}>Node tree</AppTabIndicator>
                                </div>
                                <div className="flex flex-col flex-1 bg-white p-1">
                                    <div className="flex items-stretch h-8 min-h-8">
                                        <AppButton onClick={e => window.events.emit('requestCreateNewNode')}>
                                            <FontAwesomeIcon icon={faPlus} className="text-xs opacity-50"/>
                                        </AppButton>
                                        <div className="ml-1"></div>
                                        <AppSearchInput placeholder="Search"></AppSearchInput>
                                    </div>
                                    <div className="mt-1"></div>
                                    <div className="relative h-full bg-gray-100 border">
                                        <div className="absolute flex flex-col items-stretch inset-0 overflow-auto">
                                            <div className="">
                                                <AppProductNodes/>
                                            </div>
                                            <div className="flex-1" onClick={e => actions.setSelectedNode(null)}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex flex-col flex-1">
                                <div className="flex">
                                    <AppTabIndicator active={true}>Assets</AppTabIndicator>
                                </div>
                                <div className="flex flex-col flex-1 bg-white p-1">
                                    <div className="flex items-stretch h-8 min-h-8">
                                        <AppButton>
                                            <FontAwesomeIcon icon={faPlus} className="text-xs opacity-50"/>
                                        </AppButton>
                                        <div className="ml-1"></div>
                                        <AppSearchInput placeholder="Search files"></AppSearchInput>
                                        <div className="ml-1"></div>
                                        <AppButton onClick={e => actions.scanAssets()}>
                                            <FontAwesomeIcon icon={faSync} className="text-xs opacity-50"/>
                                        </AppButton>
                                    </div>
                                    <div className="mt-1"></div>
                                    <div className="relative h-full bg-gray-100 border">
                                        <div className="absolute flex flex-col items-stretch inset-0 overflow-y-auto">
                                            <div className="">
                                                <AppAssetTree/>
                                            </div>
                                            <div className="flex-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </FlexLayout.Layout>
                    </div>
                    <div className="flex-1 py-1" style={{minWidth:'40vw'}}>
                        <div className="flex flex-col h-full">
                            <div className="flex">
                                <AppCanvasTabs/>
                            </div>
                            <div className="flex flex-col flex-1 bg-white p-1">
                                <div id="canvas-container" className="relative h-full border bg-gray-100" ref={canvasContainerRef}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div span="20" className="z-10 w-full py-1">
                        <div className="flex flex-col h-full">
                            <div className="flex">
                                <AppTabIndicator active={true}>
                                    Inspector
                                </AppTabIndicator>
                            </div>
                            <div className="flex flex-col items-stretch flex-1 bg-white p-1">
                                <div className="flex items-stretch h-8 min-h-8">
                                    <AppSearchInput placeholder="Search"></AppSearchInput>
                                </div>
                                <div className="mt-1"></div>
                                <div className="relative h-full bg-gray-100 border">
                                    <div className="absolute flex flex-col items-stretch inset-0 overflow-y-auto">
                                        <div className="p-1">
                                            <AppNodeProps/>
                                        </div>
                                        <div className="flex-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </FlexLayout.Layout>
            </div>
            <div className="z-30">
                <AppDragIndicator></AppDragIndicator>
            </div>
            <div className="z-20">
                <AppDialogs/>
            </div>
            <div id="hidden-area" className="hidden"></div>
        </div>
    );
}

ReactDOM.render(<App></App>, document.querySelector("#root"));

(function initCanvas() {
    const container = () => document.querySelector('#canvas-container');

    if(!container().offsetHeight) {
        return setTimeout(initCanvas, 200);
    }

    var stage = new Konva.Stage({
        container: 'canvas-container',
        draggable: true,
        width: container().offsetWidth,
        height: container().offsetHeight
    });
    window.stage = stage;

    var gridLayer = new Konva.Layer();
    var layer = new Konva.Layer();
    const transformLayer = new Konva.Layer();
    const transformer = new Konva.Transformer({
        rotationSnaps: [0, 90, 180, 270],
        keepRatio: true,
        enabledAnchors: [
          'top-left',
          'top-right',
          'bottom-left',
          'bottom-right',
        ]
    });
    stage.add(gridLayer);
    stage.add(layer);
    stage.add(transformLayer);
    transformLayer.add(transformer);

    stage.setPosition({
        x: 100,
        y: 100
    });

    const dragController = {
        isDragging: false,
        onDrag: null,
        init() {
            stage.on('dragstart', e => {
                if(e.target != stage || !this.onDrag) return;
                this.isDragging = true;
                const self = this;
                this.intervalID = setTimeout(function exec() {
                    if(!self.isDragging) return;
                    self.onDrag && self.onDrag();
                    setTimeout(exec, 100);
                }, 16);
            });
            stage.on('dragend', e => {
                if(e.target != stage) return;
                this.isDragging = false;
                self.onDrag && self.onDrag();
                fitStageIntoParentContainer();
            });
        }
    };
    dragController.init();

    var scaleBy = 1 / 1.1;
    function fitStageIntoParentContainer() {
        stage.width(container().offsetWidth);
        stage.height(container().offsetHeight);

        gridLayer.find('Line').forEach(line => line.destroy());

        const stagePos = stage.getPosition();
        const stageScale = stage.getScale();
        const originalPadding = 25;
        const paddingScale = Math.pow(2, Math.floor(-Math.log2(stageScale.x)));
        var padding = originalPadding * Math.max(1, paddingScale);
        for (var i = Math.floor(-stagePos.x / stageScale.x / padding); i < (container().offsetWidth  - stagePos.x) / stageScale.x / padding; i++) {
            gridLayer.add(new Konva.Line({
                points: [i * padding, (-stagePos.y - stage.height()) / stageScale.y, i * padding, (-stagePos.y + stage.height()  + container().offsetHeight) / stageScale.y],
                stroke: i == 0 ? '#0c0' : '#ddd',
                strokeWidth: (i == 0 ? 1 : 1) * paddingScale,
            }));
        }

        gridLayer.add(new Konva.Line({points: [0,0,10,10]}));
        for (var j = Math.floor(-stagePos.y / stageScale.y / padding); j < (container().offsetHeight - stagePos.y) / stageScale.y / padding; j++) {
            gridLayer.add(new Konva.Line({
                points: [(-stagePos.x - stage.width()) / stageScale.x, j * padding, (-stagePos.x + stage.width() + container().offsetWidth) / stageScale.x, j * padding],
                stroke: j == 0 ? '#d7982a' : '#ddd',
                strokeWidth: (j == 0 ? 1 : 1) * paddingScale,
            }));
        }
    }

    const lockedObjects = [];
    events.on('requestLockObject', obj => {
        obj.setAttrs({
            listening: false
        });
        if(!lockedObjects.includes(obj)) {
            lockedObjects.push(obj);
        }
    });

    stage.on('click tap', function(e) {
        const tr = transformer;

        if(e.target.parent == tr) return;

        // if click on empty area - remove all selections
        if (e.target === stage || lockedObjects.includes(e.target)) {
            events.emit('canvasObjectUnselected');
            tr.nodes().forEach(node => node.setAttrs({ draggable: false }));
            tr.nodes([]);
            return;
        }

        const isSelected = tr.nodes().indexOf(e.target) >= 0;

        if (!isSelected) {
            tr.nodes().forEach(node => node.setAttrs({ draggable: false }));
            tr.nodes([e.target]);
            tr.nodes().forEach(node => node.setAttrs({ draggable: true }));
            events.emit('canvasObjectSelected', e.target);
        } else {
            const nodes = tr.nodes().slice();
            tr.nodes().forEach(node => node.setAttrs({ draggable: false }));
            nodes.splice(nodes.indexOf(e.target), 1);
            tr.nodes(nodes);
            tr.nodes().forEach(node => node.setAttrs({ draggable: true }));
            events.emit('canvasObjectUnselected', e.target);
        }
    });

    events.on('requestCanvasSelect', obj => {
        const tr = transformer;
        tr.nodes().forEach(node => node.setAttrs({ draggable: false }));
        tr.nodes([obj]);
        tr.nodes().forEach(node => node.setAttrs({ draggable: true }));
    });

    events.on('requestCanvasUnselect', obj => {
        const tr = transformer;
        if(obj) {
            tr.nodes(tr.nodes.filter(node => node != obj));
            obj.setAttrs({
                draggable: false
            });
        }
        else {
            tr.nodes().forEach(node => node.setAttrs({ draggable: false }));
            tr.nodes([]);
        }
    });

    fitStageIntoParentContainer();
    window.addEventListener('resize', fitStageIntoParentContainer);
    dragController.onDrag = fitStageIntoParentContainer;
    stage.on('wheel', (e) => {
        e.evt.preventDefault();
        var oldScale = stage.scaleX();

        var pointer = stage.getPointerPosition();

        var mousePointTo = {
            x: (pointer.x - stage.x()) / oldScale,
            y: (pointer.y - stage.y()) / oldScale,
        };

        var newScale =
            e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

        stage.scale({ x: newScale, y: newScale });

        var newPos = {
            x: pointer.x - mousePointTo.x * newScale,
            y: pointer.y - mousePointTo.y * newScale,
        };
        stage.position(newPos);
        fitStageIntoParentContainer();
    });

    const lastContainerSize = {
        width: container().offsetWidth,
        height: container().offsetHeight
    }
    setInterval(() => {
        if(container().offsetWidth != lastContainerSize.width || container().offsetHeight != lastContainerSize.height) {
            fitStageIntoParentContainer();
        }
        lastContainerSize.width = container().offsetWidth;
        lastContainerSize.height = container().offsetHeight;
    }, 200);

    events.on('requestSetCanvasScale', scale => {
        stage.scale({
            x: scale,
            y: scale
        });
        fitStageIntoParentContainer();
    });

    events.emit('stageReady');
})();

window.onbeforeunload = function() {
    if(false)
        return "Be sure to save your progress before exit!";
};

Object.entries(window.handlers).forEach(([key, handlers]) => {
    window.events.register(key);
    window.events.unbind(key);
    window.events.on(key, (...args) => {
        Object.values(window.handlers[key]).forEach(handler => handler(...args));
    });
});

const dev = {
    launchDemoDialog() {
        const dialogID = actions.openDialog({
            title: 'Demo dialog',
            body: '<p>Hello</p>'
        });
        const dialogData = actions.getResource(store.dialogs, dialogID);
        actions.updateDialog(dialogID, {
            body: dialogData.body + '<p>This line is dynamically appended.</p>'
        });
    },
    openNewTab() {
        actions.addTab({
            title: 'New tab ' + uuidv4().slice(0, 4),
        });
    }
}
window.dev = dev;

(function devFunc() {
    for(const k in dev) {
        actions.devRegister(k, dev[k]);
    }

    const PluginList = observer(props => {
        const [state, setState] = useState({
            selectedPluginName: null
        });
        return <>
            {
                store.plugins.map(({name, instance}) => {
                    return <span
                        key={name}
                        className={`px-1 ${state.selectedPluginName == name ? 'bg-gray-200' : 'hover:bg-gray-100'}`}
                        onClick={e => setState({selectedPluginName: name})}
                    >{name.replace('.js', '')}</span>
                })
            }
        </>
    });

    function DevSelect(props) {
        const values = props.values ?? [];
        const [state, setState] = useState({
            selectedValue: null,
            show: false
        });
        const togglerRef = useRef(null);
        const selectedTitle = values.filter(v => v.value == state.selectedValue)[0]?.title ?? 'Select an option';
        const handleClick = (e) => {
            if(e.target == togglerRef.current || togglerRef.current.contains(e.target)) {
                setState({
                    ...state,
                    show: !state.show
                }); 
            }
        };
        const handleSelect = v => {
            const isChanged = v.value != state.selectedValue;
            setState({
                ...state,
                selectedValue: v.value,
                show: false,
            });
            isChanged && props.onChange && props.onChange(v.value);
        };
        return <div className="bg-white p-2">
            <div className="flex h-6">
                <div className="flex-1 min-w-1/2">
                    { props.label }
                </div>
                <div className="relative flex-1 w-1/2 bg-gray-200" onClick={handleClick}>
                    <div ref={togglerRef} className="relative w-full pl-2 px-6 bg-gray-200 outline-none truncate cursor-pointer">
                        <span>{selectedTitle}</span>
                        <FontAwesomeIcon icon={faChevronDown} className="absolute right-2 top-0 h-full text-xs opacity-50"/>
                    </div>
                    <div className={`absolute w-full p-2 bg-white ${state.show ? '' : 'hidden'}`}>
                        { values.map(v => 
                            <div className={`hover:bg-gray-100 ${v.value == state.selectedValue ? 'bg-gray-100' : ''}`} onClick={e => handleSelect(v)}>{v.title}</div>
                        ) }
                    </div>
                </div>
            </div>
        </div>
    }

    const dialogID = actions.openDialog({
        title: 'Developing content',
        body: <DevSelect label="Demo select" values={[
                {
                    value: 'option1',
                    title: 'Option 1'
                },
                {
                    value: 'option2',
                    title: 'Option 2'
                }
            ]}/>
    });
    actions.closeDialog(dialogID);
})();

core.pluginSystem.default.init();
