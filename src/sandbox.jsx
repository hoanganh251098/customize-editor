import React, { useRef } from 'react';
import ReactDOM from 'react-dom';
import './styles/tailwind.css';
import FlexLayout from './components/flexlayout';
import Konva from 'konva';

function App() {
    return <FlexLayout.Container>
        <FlexLayout.Layout vertical={true}>
            <FlexLayout.Child className="bg-gray-200" style={{minWidth:'200px'}}></FlexLayout.Child>
            <FlexLayout.Child span="33" className="border flex justify-center items-center">
                <p>Flex layout demo</p>
            </FlexLayout.Child>
            <FlexLayout.Layout className="border">
                <FlexLayout.Child className="bg-red-100" style={{minWidth:'200px'}}></FlexLayout.Child>
                <FlexLayout.Child className="bg-green-100" style={{minWidth:'200px'}}></FlexLayout.Child>
                <FlexLayout.Child className="bg-blue-100" style={{minWidth:'200px'}}></FlexLayout.Child>
                <FlexLayout.Child className="bg-gray-200" style={{minWidth:'200px'}}></FlexLayout.Child>
            </FlexLayout.Layout>
        </FlexLayout.Layout>
    </FlexLayout.Container>;
}

ReactDOM.render(<App/>, document.querySelector('#root'));
