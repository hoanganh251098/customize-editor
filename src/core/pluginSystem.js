import * as utils from './utils.js';
import React from 'react';
import { action as registerAction, extendObservable } from "mobx";
import $ from 'jquery';
import Konva from 'konva';

const isNodeWebkit = utils.isNodeWebkit();

const _exports = {
    get availablePlugins() {
        if(!isNodeWebkit) return;
        const fs = nw.require('fs');
        return fs.readdirSync('./plugins').map(pluginName => pluginName.replace('.js', ''));
    },
    loadPlugin(name) {
        let plugin;
        try {
            plugin = nw.require(`./plugins/${name}`);
        }
        catch(e) { console.error(e); }
        if(!plugin) return;
        const apiObj = {
            store: window.store,
            actions: window.actions,
            registerAction: registerAction,
            extendObservable: extendObservable,
            get stage() { return window.stage; },
            core: window.core,
            events: window.events,
            React: React,
            Konva: Konva,
            html(...args) {
                const element = React.createElement('div', {dangerouslySetInnerHTML: { __html: args[0] }});
                return element;
            },
            $: $,
            uuidv4: core.uuidv4.default({useRegistry: false}),
            document: document,
            window: window
        };
        plugin.api = apiObj;
        plugin.onInit && plugin.onInit();
        return plugin;
    },
    init() {
        if(!isNodeWebkit) return;
        const win = nw.Window.get();
        const fs = nw.require('fs');
        window.win = win;
        win.maximize();
        // win.showDevTools();
        const activePlugins = JSON.parse(fs.readFileSync('./__data/active-plugins.json').toString());
        for(const pluginName of activePlugins) {
            if(!_exports.availablePlugins.includes(pluginName)) continue;
            const plugin = _exports.loadPlugin(pluginName);
            actions.registerPlugin(pluginName, plugin);
        }
    }
};

export default _exports;

