import $ from 'jquery';

function arrayWrap(obj) {
    if(obj == null || obj == undefined || typeof obj == 'string' || typeof obj.length != 'number') {
        return [obj];
    }
    return obj;
}

function filterObj(props, blacklist) {
    const copied = {...props};
    for(const prop of blacklist) {
        delete copied[prop];
    }
    return copied;
}

function isNodeWebkit() {
    return window.nw != undefined;
}

function parseCamel(text) {
    return text.match(/([A-Z]?[a-z]+|[0-9]+)/g);
}

function clearObject(obj) {
    for(const k in obj) {
        delete obj[k];
    }
}

const document = window.document;
function loadFont(name, url, callback) {
    if(!loadFont.__private) {
        loadFont.__private = {};
    }
    const hash = (str) => {
        let result = '';
        for (const [cid, c] of [...str].entries()) {
            result += c.charCodeAt(0) * (cid + 1);
        }
        return (+result) % 123456789101112;
    };
    const fontLoaderClass = `font-loader-${hash(name)}`;
    if ($(document).find(`.${fontLoaderClass}`).length == 0) {
        $(document).find('body').append(`
        <div class="${fontLoaderClass}">
            <style>
                @font-face {
                    font-family: ${JSON.stringify(name)};
                    src: url(${JSON.stringify(url)});
                }
            </style>
        </div>
        `);
    }
    const font = new FontFaceObserver(name);
    if(!loadFont.__private.fontloader) loadFont.__private.fontloader = {};
    const promise = font.load(null, 10000);
    if(!loadFont.__private.fontloader[name]) {
        promise.catch(e => {
            console.error(`Failed to load font '${name}': ${e}`);
        });
        loadFont.__private.fontloader[name] = true;
    }
    callback && promise.then(callback);
}

export { arrayWrap, filterObj, isNodeWebkit, parseCamel, clearObject, loadFont };