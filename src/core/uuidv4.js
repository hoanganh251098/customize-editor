export default function generator({ useRegistry=true }={}) {
    const registered = {};
    return function () {
        let uuid = null;
        while(uuid == null || (useRegistry && registered[uuid])) {
            uuid = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }
        if(useRegistry) {
            registered[uuid] = true;
        }
        return uuid;
    }
};
