module.exports = {
    onShow() {
        const {store, actions, React} = this.api;
        actions.openDialog({
            title: 'ATeam Customize Loader',
            body: ` 
                <span>
                    <span class="italic">
                        Xin chào 
                    </span>😃
                </span>
            `
        });
    },
    onInit() {
        const {uuidv4, core, store, actions, registerAction, events, Konva, $ } = this.api;
        const textPath = require('./textpath.js');
        const fs = require('fs');
        const path = require('path');
        let nodeConfigMap = {};
        let songMaskNodeID = null;
        function findNode(nodeID, node=store.document) {
            if(node._id == nodeID) return node;
            for(const childNode of node.children ?? []) {
                const result = findNode(nodeID, childNode);
                if(result) return result;
            }
        }
        function observeNodeProp(nodeID, propType, propName, callback, interval=500) {
            let prevValue;
            const findNode = (node=store.document) => {
                if(node._id == nodeID) return node;
                for(const childNode of node.children ?? []) {
                    const result = findNode(childNode);
                    if(result) return result;
                }
            };
            const intervalID = setInterval(() => {
                const node = findNode();
                const value = node[propType][propName].value;
                if(value != prevValue) {
                    callback && callback(value, prevValue);
                    prevValue = value;
                }
            }, interval);
            return intervalID;
        }
        function observeNodeProps(nodeID, props, callback, interval=500) {
            const prevValues = {
                commonData: {},
                data: {}
            };
            const intervalID = setInterval(() => {
                const node = findNode(nodeID);
                const changes = {
                    commonData: {},
                    data: {}
                };
                let hasChanged = false;
                for(const prop of props) {
                    const [propGroup, propName] = prop.split('.');
                    const prevValue = prevValues[propGroup][propName];
                    const currentValue = node[propGroup][propName].value;
                    if(prevValue != currentValue) {
                        prevValues[propGroup][propName] = currentValue;
                        hasChanged = true;
                    }
                    changes[propGroup][propName] = {
                        value: currentValue,
                        prevValue: prevValue
                    };
                }
                if(hasChanged) {
                    callback && callback(changes);
                }
            }, interval);
            return intervalID;
        }
        function stopObserve(intervalID) {
            clearInterval(intervalID);
        }
        const parseLayerType = (layerType) => {
            const metaRaw = (layerType.match(/\[.*/g) ?? [''])[0];
            const meta = {};
            const result = {
                type: layerType.replace(metaRaw, ''),
                meta: meta
            };
            for (const fieldSet of [...metaRaw.matchAll(/[a-zA-Z0-9_-]+\:[\/$a-zA-Z0-9_-\s]+/g)]) {
                const [key, value] = fieldSet[0].split(':');
                meta[key] = value;
            }
            return result;
        };
        const rgbToHex = (r, g, b, a) => {
            const red = ('0' + (+(r ?? 0)).toString(16)).slice(-2);
            const green = ('0' + (+(g ?? 0)).toString(16)).slice(-2);
            const blue = ('0' + (+(b ?? 0)).toString(16)).slice(-2);
            const alpha = ('0' + (+(a ?? 0)).toString(16)).slice(-2);
            if (a == undefined) {
                return '#' + red + green + blue;
            }
            return '#' + red + green + blue + alpha;
        };
        function hexToRGB(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})?$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16),
                a: isNaN(parseInt(result[4], 16)) ? 255 : parseInt(result[4], 16)
            } : null;
        }
        const bulkAssign = (receiver, valueMap) => {
            for(const k in valueMap) {
                Object.assign(receiver[k], {
                    value: valueMap[k]
                });
            }
        };
        const refreshInspector = () => {
            const backup = store.selectedNodeID;
            store.selectedNodeID = null;
            store.selectedNodeID = backup;
        };
        const getSpiralTextBase64 = ({
            width=100,
            height=100,
            lineHeight=1,
            start=600,
            end=18000,
            text='',
            letterSpacing=2,
            color='#000',
            fontSize=20,
            fontFamily='Georgia'
        }={}) => {
            if($('#spiralCanvas').length == 0) {
                $('body').append('<div class="hidden"><canvas id="spiralCanvas"></canvas></div>');
            }

            const path = [];
            const trueLineHeight = fontSize / 4 * lineHeight;
            for (let i = 0; i < 7200; i++) {
                const angle = 0.1 * i;
                const deg = angle / Math.PI * 180;
                if(deg >= start && deg <= end) {
                    const x = (trueLineHeight + trueLineHeight * angle) * Math.cos(angle);
                    const y = (trueLineHeight + trueLineHeight * angle) * Math.sin(angle);
                    path.push(x);
                    path.push(y);
                }
            }

            const canvas = $("#spiralCanvas")[0];
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            function drawPath(path) {
                ctx.save();
                ctx.strokeStyle = '#00000000';
                ctx.lineWidth = 1;
                ctx.beginPath();
                ctx.moveTo(path[0], path[1]);
                for (var i = 2; i < path.length; i += 2) {
                    ctx.lineTo(path[i], path[i + 1]);
                }
                ctx.stroke();
                ctx.restore();
            }

            ctx.font = `${fontSize}px ${fontFamily}`;
            ctx.textBaseline = "middle";
            ctx.fillStyle = color;
            ctx.lineWidth = letterSpacing;
            ctx.strokeStyle = "#00000000";

            ctx.translate(width / 2, height / 2);
            drawPath(path);
            textPath.init(ctx.constructor);
            ctx.textPath(text, path);

            return canvas.toDataURL();
        }
        /*
        types:
            input       => string
            textarea    => string
            number      => number
            checkbox    => bool
            select      => string
            colorpicker => string
        */
        const schemas = {
            text: {
                module: {
                    label: 'Module',
                    type: 'input'
                },
                role: {
                    label: 'Role',
                    type: 'input'
                },
                required: {
                    label: 'Required',
                    type: 'checkbox',
                    displayValues: ['True', 'False'],
                    value: false
                },
                label: {
                    label: 'Label',
                    type: 'input',
                },
                defaultText: {
                    label: 'Default text',
                    type: 'textarea'
                },
                color: {
                    label: 'Color',
                    type: 'colorpicker',
                    value: '#000000'
                },
                fontName: {
                    label: 'Font name',
                    type: 'input'
                },
                fontPath: {
                    label: 'Font path',
                    type: 'input',
                    availableValues: []
                },
                fontSize: {
                    label: 'Font size',
                    type: 'number'
                },
                vAlign: {
                    label: 'V-Alignment',
                    type: 'select',
                    values: [
                        {
                            value: 'top',
                            title: 'Top'
                        },
                        {
                            value: 'middle',
                            title: 'Middle'
                        },
                        {
                            value: 'bottom',
                            title: 'Bottom'
                        },
                    ]
                },
                hAlign: {
                    label: 'H-Alignment',
                    type: 'select',
                    values: [
                        {
                            value: 'left',
                            title: 'Left'
                        },
                        {
                            value: 'center',
                            title: 'Center'
                        },
                        {
                            value: 'right',
                            title: 'Right'
                        }
                    ]
                }
            },
            songMask: {
                autoSyncPosition: {
                    label: 'Auto sync position',
                    type: 'checkbox',
                    value: true
                }
            },
            songLyrics: {
                lyrics: {
                    label: 'Lyrics',
                    type: 'textarea',
                    value: ''
                },
                color: {
                    label: 'Color',
                    type: 'colorpicker',
                    value: '#000000'
                },
                fontPath: {
                    label: 'Font path(http)',
                    type: 'input',
                    value: ''
                },
                shape: {
                    label: 'Shape',
                    type: 'select',
                    values: [
                        { value: 'heart', title: 'heart' },
                        { value: 'heart2', title: 'heart2' },
                        { value: 'heart3', title: 'heart3' },
                        { value: 'rect1', title: 'rect1' },
                    ],
                    value: 'heart'
                },
                lineHeight: {
                    label: 'Line height',
                    type: 'number',
                    value: 1
                }
            },
            spiralText: {
                text: {
                    label: 'Text',
                    type: 'textarea',
                    value: ''
                },
                startRadius: {
                    label: 'Start radius',
                    type: 'number',
                    value: 0
                },
                fontPath: {
                    label: 'Font path',
                    type: 'input',
                    value: ''
                },
                fontSize: {
                    label: 'Font size',
                    type: 'number',
                    value: 0
                },
                color: {
                    label: 'Color',
                    type: 'colorpicker',
                    value: '#000000ff'
                },
                characterLimit: {
                    label: 'Character limit',
                    type: 'number',
                    value: 1000
                },
                version: {
                    label: 'Version',
                    type: 'hidden',
                    value: 'initial-version'
                }
            }
        };
        const handleAssetReady = () => {
            registerAction(() => {
                nodeConfigMap = {};
                const localEvents = new core.eventSystem.EventEmitter;
                localEvents.register('selectedNodeChange');
                const stage = this.api.stage;
                const layer = stage.getLayers()[1];
                const dataJSONpath = path.join(store.assetRoot, 'data.json');
                const dataJSON = JSON.parse(fs.readFileSync(dataJSONpath));
                const documentNode = actions.createDocumentNode({
                    title: 'Document',
                    schema: {
                        resolution: {
                            label: 'Resolution',
                            type: 'number'
                        }
                    },
                    children: []
                });
                Object.assign(documentNode.commonData.width, {
                    value: dataJSON.document.width
                });
                Object.assign(documentNode.commonData.height, {
                    value: dataJSON.document.height
                });
                Object.assign(store.document, documentNode);
                Object.assign(store.document.data.resolution, {
                    value: dataJSON.document.resolution
                });

                events.emit('requestSetCanvasScale', Math.min(stage.width() / dataJSON.document.width, stage.height() / dataJSON.document.height) / 1.5);
                const documentRect = new Konva.Rect({
                    stroke: '#d130fd',
                    fill: '#0000000f',
                    x: 0,
                    y: 0,
                    width: dataJSON.document.width,
                    height: dataJSON.document.height,
                });
                layer.add(documentRect);
                events.emit('requestLockObject', documentRect);

                for(const [layerIdx, docLayer] of dataJSON.children.entries()) {
                    const layerData = {
                        ...parseLayerType(docLayer.type),
                        config: docLayer
                    };
                    const setCommonData = (node) => {
                        bulkAssign(node.commonData, {
                            top: layerData.config.top ?? 0,
                            left: layerData.config.left ?? 0,
                            width: layerData.config.width ?? 0,
                            height: layerData.config.height ?? 0,
                            rotation: layerData.config.rotate ?? 0,
                            opacity: layerData.config.opacity ?? 1,
                            visibility: layerData.config.visible ?? true,
                            keepRatio: layerData.config.keepRatio
                        });
                    };
                    if(layerData.type == 'text' && layerData.meta.module != 'song_lyrics') {
                        const node = actions.createDocumentNode({
                            title: 'Text',
                            type: 'text',
                            schema: schemas.text,
                            helpers: {
                                roundBoxProps: {
                                    title: 'Round box properties',
                                    onClick: () => {
                                        const nodeData = findNode(node._id);
                                        for(const k in nodeData.commonData) {
                                            if(nodeData.commonData[k].type != 'number') continue;
                                            Object.assign(nodeData.commonData[k], {
                                                value: Math.round(nodeData.commonData[k].value)
                                            });
                                        }
                                    }
                                },
                                validateFontSize: {
                                    title: 'Validate font size',
                                    onClick: () => {
                                        const nodeData = findNode(node._id);
                                        if(nodeData.data.fontSize.value > nodeData.commonData.height.value) {
                                            Object.assign(nodeData.data.fontSize, {
                                                value: nodeData.commonData.height.value
                                            });
                                        }
                                    }
                                }
                            }
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        setCommonData(node);
                        bulkAssign(node.data, {
                            module: layerData.meta.module,
                            role: layerData.meta.role ?? '',
                            required: layerData.meta.required == 'true',
                            label: layerData.meta.label,
                            defaultText: layerData.config.text.value,
                            color: rgbToHex(...layerData.config.text.font.colors[0]),
                            fontName: layerData.config.text.font.name,
                            fontPath: layerData.config.text.font.path,
                            fontSize: layerData.config.text.font.sizes[0],
                            vAlign: layerData.meta.valign ?? 'top',
                            // vAlign: 'middle',
                            hAlign: layerData.config.text.font.alignment[0]
                        });
                        store.document.children.push(node);
                        const textObj = new Konva.Text({
                            text: '',
                            wrap: 'char',
                        });
                        layer.add(textObj);
                        textObj.on('dragend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: textObj.attrs.x - textObj.attrs.width / 2
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: textObj.attrs.y - textObj.attrs.height / 2
                            });
                            refreshInspector();
                        }));
                        textObj.on('transformend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: textObj.attrs.x - textObj.attrs.width / 2
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: textObj.attrs.y - textObj.attrs.height / 2
                            });
                            Object.assign(nodeData.commonData.width, {
                                value: textObj.attrs.width
                            });
                            Object.assign(nodeData.commonData.height, {
                                value: textObj.attrs.height
                            });
                            Object.assign(nodeData.commonData.rotation, {
                                value: textObj.attrs.rotation
                            });
                            refreshInspector();
                        }));
                        events.on('canvasObjectSelected', obj => {
                            if(obj != textObj) return;
                            actions.setSelectedNode(node._id);
                        });
                        const fontPath = layerData.config.text.font.path;
                        core.utils.loadFont(layerData.config.text.font.name,
                            fontPath.slice(0, 5).includes('http') ? fontPath : path.join(store.assetRoot, fontPath),
                            () => {
                                const nodeData = findNode(node._id);
                                textObj.setAttrs({
                                    fontFamily: layerData.config.text.font.name,
                                    align: nodeData.data.vAlign.value
                                });
                            });
                        localEvents.on('selectedNodeChange', () => {
                            if(store.selectedNodeID == node._id) {
                                events.emit('requestCanvasSelect', textObj);
                            }
                        });
                        observeNodeProps(node._id, 
                            [
                                'commonData.left',
                                'commonData.top',
                                'commonData.width',
                                'commonData.height',
                                'commonData.rotation',
                                'data.defaultText',
                                'data.fontSize',
                                'data.color',
                                // 'data.vAlign',
                                'data.hAlign',
                                'data.label',
                            ], changes => {
                                const attributes = {};
                                for(const k in changes.commonData) {
                                    const value = changes.commonData[k].value;
                                    if(k == 'left') attributes.x = +value;
                                    if(k == 'top') attributes.y = +value;
                                    if(k == 'width') attributes.width = +value;
                                    if(k == 'height') attributes.height = +value;
                                    if(k == 'rotation') attributes.rotation = +value;
                                }
                                for(const k in changes.data) {
                                    const value = changes.data[k].value;
                                    if(k == 'defaultText') attributes.text = value;
                                    if(k == 'fontSize') attributes.fontSize = +value;
                                    if(k == 'color') attributes.fill = value;
                                    // if(k == 'vAlign') attributes.verticalAlign = value;
                                    if(k == 'hAlign') attributes.align = value;
                                    if(k == 'label') {
                                        const nodeData = findNode(node._id);
                                        Object.assign(nodeData, {
                                            title: `Text - ${value ?? ''}`
                                        });
                                    }
                                }
                                textObj.setAttrs({
                                    scale: { x: 1, y: 1 },
                                    verticalAlign: 'middle',
                                    ...attributes,
                                    x: attributes.x + attributes.width / 2,
                                    y: attributes.y + attributes.height / 2,
                                    offsetX: attributes.width / 2,
                                    offsetY: attributes.height / 2
                                });
                            }
                        );
                    }
                    else if(layerData.type == 'fg') {
                        const node = actions.createDocumentNode({
                            title: 'Foreground',
                            type: 'static-image',
                            schema: schemas.staticImage
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        setCommonData(node);
                        store.document.children.push(node);
                        const imgObj = new Image();
                        const imgSrc = layerData.config.preview.includes('http')
                            ? layerData.config.preview
                            : path.join(store.assetRoot, layerData.config.preview)
                            ;
                        const konvaImageObj = new Konva.Image({});
                        imgObj.onload = () => {
                            konvaImageObj.setAttrs({
                                image: imgObj,
                                left: layerData.config.left,
                                top: layerData.config.top,
                                width: layerData.config.width,
                                height: layerData.config.height
                            });
                        };
                        imgObj.src = imgSrc;
                        layer.add(konvaImageObj);
                        events.emit('requestLockObject', konvaImageObj);
                        observeNodeProps(node._id, 
                            [
                                'commonData.left',
                                'commonData.top',
                                'commonData.width',
                                'commonData.height',
                                'commonData.rotation',
                            ], changes => {
                                const attributes = {};
                                for(const k in changes.commonData) {
                                    const value = changes.commonData[k].value;
                                    if(k == 'left') attributes.x = +value;
                                    if(k == 'top') attributes.y = +value;
                                    if(k == 'width') attributes.width = +value;
                                    if(k == 'height') attributes.height = +value;
                                    if(k == 'rotation') attributes.rotation = +value;
                                }
                                konvaImageObj.setAttrs(attributes);
                            }
                        );
                    }
                    else if(layerData.type == 'text' && layerData.meta.module == 'song_lyrics') {
                        const node = actions.createDocumentNode({
                            title: 'Song lyrics',
                            type: 'song-lyrics',
                            schema: schemas.songLyrics,
                            helpers: {
                                fillDemoLyrics: {
                                    title: 'Fill demo lyrics',
                                    onClick: () => {
                                        const nodeData = findNode(node._id);
                                        nodeData.data.lyrics.value = `I've been alone with you inside my mind And in my dreams, I've kissed your lips a thousand times I sometimes see you pass outside my door Hello, is it me you're looking for? I can see it in your eyes I can see it in your smile You're all I've ever wanted And my arms are open wide 'Cause you know just what to say And you know just what to do And I want to tell you so much I love you`;
                                    }
                                }
                            }
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        setCommonData(node);
                        store.document.children.push(node);
                        const imgObj = new Image();
                        const konvaImageObj = new Konva.Image({});
                        imgObj.onload = () => {
                            const nodeData = findNode(node._id);
                            konvaImageObj.setAttrs({
                                image: imgObj,
                                left: +nodeData.commonData.left.value,
                                top: +nodeData.commonData.top.value,
                                width: +nodeData.commonData.width.value,
                                height: +nodeData.commonData.height.value
                            });
                        };
                        node.data.lyrics.value = layerData.config.text.value;
                        node.data.shape.value = layerData.meta.shape ?? 'heart';
                        node.data.lineHeight.value = layerData.config.text.font.lineHeight ?? 1;
                        node.data.color.value = rgbToHex(...layerData.config.text.font.colors[0]);
                        node.data.fontPath.value = layerData.config.text.font.path;
                        layer.add(konvaImageObj);
                        konvaImageObj.on('dragend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: konvaImageObj.attrs.x
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: konvaImageObj.attrs.y
                            });
                            refreshInspector();
                        }));
                        konvaImageObj.on('transformend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: konvaImageObj.attrs.x
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: konvaImageObj.attrs.y
                            });
                            Object.assign(nodeData.commonData.width, {
                                value: konvaImageObj.attrs.width
                            });
                            Object.assign(nodeData.commonData.height, {
                                value: konvaImageObj.attrs.height
                            });
                            Object.assign(nodeData.commonData.rotation, {
                                value: konvaImageObj.attrs.rotation
                            });
                            refreshInspector();
                        }));
                        events.on('canvasObjectSelected', obj => {
                            if(obj != konvaImageObj) return;
                            actions.setSelectedNode(node._id);
                        });
                        localEvents.on('selectedNodeChange', () => {
                            if(store.selectedNodeID == node._id) {
                                events.emit('requestCanvasSelect', konvaImageObj);
                            }
                        });
                        let changeID = null;
                        observeNodeProps(node._id, 
                            [
                                'commonData.left',
                                'commonData.top',
                                'commonData.width',
                                'commonData.height',
                                'commonData.rotation',
                                'commonData.keepRatio',
                                'data.lyrics',
                                'data.color',
                                'data.fontPath',
                                'data.shape',
                                'data.lineHeight'
                            ], changes => {
                                const localChangeID = uuidv4();
                                changeID = localChangeID;
                                const attributes = {};
                                for(const k in changes.commonData) {
                                    const value = changes.commonData[k].value;
                                    if(k == 'left') attributes.x = +value;
                                    if(k == 'top') attributes.y = +value;
                                    if(k == 'width') attributes.width = +value;
                                    if(k == 'height') attributes.height = +value;
                                    if(k == 'rotation') attributes.rotation = +value;
                                }
                                const renderLyrics = () => {
                                    const lyrics = changes.data.lyrics.value;
                                    const color = changes.data.color.value;
                                    const fontPath = changes.data.fontPath.value;
                                    const shape = changes.data.shape.value;
                                    const lineHeight = changes.data.lineHeight.value;
                                    imgObj.src = '';
                                    $.ajax({
                                        url: 'https://dev.goodsmize.com/text-shape/generate',
                                        type: 'post',
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        data: JSON.stringify({
                                            lyrics,
                                            color,
                                            font: fontPath,
                                            shape,
                                            lineHeight,
                                            base64: true,
                                            export: false,
                                            width: 500
                                        })
                                    })
                                    .done(res => {
                                        if(localChangeID != changeID) return;
                                        const url = res.data;
                                        imgObj.src = url;
                                    });
                                };
                                if(changes.data.lyrics || changes.data.color || changes.data.fontPath
                                    || changes.data.shape || changes.data.lineHeight) {
                                    renderLyrics();
                                }
                                konvaImageObj.setAttrs(attributes);
                                if(songMaskNodeID) {
                                    const songMaskNode = findNode(songMaskNodeID);
                                    if(songMaskNode.data.autoSyncPosition.value) {
                                        bulkAssign(songMaskNode.commonData, {
                                            left: changes.commonData.left.value,
                                            top: changes.commonData.top.value,
                                            width: changes.commonData.width.value,
                                            height: changes.commonData.height.value,
                                            keepRatio: changes.commonData.keepRatio.value
                                        });
                                    }
                                }
                            }
                        );
                    }
                    else if(layerData.type == 'custom' && layerData.meta.module == 'spiral') {
                        const node = actions.createDocumentNode({
                            title: 'Spiral text',
                            type: 'spiralText',
                            schema: schemas.spiralText,
                            helpers: {
                                fillDemoLyrics: {
                                    title: 'Fill demo lyrics',
                                    onClick: () => {
                                        const nodeData = findNode(node._id);
                                        nodeData.data.text.value = `I've been alone with you inside my mind And in my dreams, I've kissed your lips a thousand times I sometimes see you pass outside my door Hello, is it me you're looking for? I can see it in your eyes I can see it in your smile You're all I've ever wanted And my arms are open wide 'Cause you know just what to say And you know just what to do And I want to tell you so much I love you`;
                                    }
                                }
                            }
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        setCommonData(node);
                        store.document.children.push(node);
                        const imgObj = new Image();
                        const konvaImageObj = new Konva.Image({});
                        imgObj.onload = () => {
                            const nodeData = findNode(node._id);
                            konvaImageObj.setAttrs({
                                image: imgObj,
                                left: +nodeData.commonData.left.value,
                                top: +nodeData.commonData.top.value,
                                width: +nodeData.commonData.width.value,
                                height: +nodeData.commonData.height.value
                            });
                        };
                        node.data.text.value            = layerData.config.spiral.text;
                        node.data.startRadius.value     = +(layerData.config.spiral.startRadius ?? 0);
                        node.data.fontSize.value        = +(layerData.config.spiral.fontSize ?? 0);
                        node.data.fontPath.value        = layerData.config.spiral.fontPath ?? '';
                        node.data.color.value           = layerData.config.spiral.color ?? '#000000ff';
                        node.data.characterLimit.value  = +(layerData.config.spiral.characterLimit ?? 1000);
                        layer.add(konvaImageObj);
                        konvaImageObj.on('dragend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: konvaImageObj.attrs.x
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: konvaImageObj.attrs.y
                            });
                            refreshInspector();
                        }));
                        konvaImageObj.on('transformend', registerAction(() => {
                            const nodeData = findNode(node._id);
                            Object.assign(nodeData.commonData.left, {
                                value: konvaImageObj.attrs.x
                            });
                            Object.assign(nodeData.commonData.top, {
                                value: konvaImageObj.attrs.y
                            });
                            Object.assign(nodeData.commonData.width, {
                                value: konvaImageObj.attrs.width
                            });
                            Object.assign(nodeData.commonData.height, {
                                value: konvaImageObj.attrs.height
                            });
                            Object.assign(nodeData.commonData.rotation, {
                                value: konvaImageObj.attrs.rotation
                            });
                            refreshInspector();
                        }));
                        events.on('canvasObjectSelected', obj => {
                            if(obj != konvaImageObj) return;
                            actions.setSelectedNode(node._id);
                        });
                        localEvents.on('selectedNodeChange', () => {
                            if(store.selectedNodeID == node._id) {
                                events.emit('requestCanvasSelect', konvaImageObj);
                            }
                        });
                        observeNodeProps(node._id, 
                            [
                                'commonData.left',
                                'commonData.top',
                                'commonData.width',
                                'commonData.height',
                                'commonData.rotation',
                                'commonData.keepRatio',
                                'data.text',
                                'data.startRadius',
                                'data.fontPath',
                                'data.fontSize',
                                'data.color',
                                'data.characterLimit',
                                'data.version'
                            ], changes => {
                                const localChangeID = uuidv4();
                                const attributes = {};
                                for(const k in changes.commonData) {
                                    const value = changes.commonData[k].value;
                                    if(k == 'left') attributes.x = +value;
                                    if(k == 'top') attributes.y = +value;
                                    if(k == 'width') attributes.width = +value;
                                    if(k == 'height') attributes.height = +value;
                                    if(k == 'rotation') attributes.rotation = +value;
                                }
                                const renderLyrics = () => {
                                    const characterLimit = +changes.data.characterLimit.value;
                                    console.log(characterLimit);
                                    const limitReached  = changes.data.text.value.length > characterLimit;
                                    const text          = changes.data.text.value.slice(0, characterLimit) + (limitReached ? '...':'');
                                    const startRadius   = +changes.data.startRadius.value;
                                    const fontPath      = changes.data.fontPath.value;
                                    const fontSize      = +changes.data.fontSize.value;
                                    const color         = changes.data.color.value;
                                    const width         = +changes.commonData.width.value;
                                    const height        = +changes.commonData.height.value;
                                    imgObj.src = getSpiralTextBase64({
                                        width,
                                        height,
                                        text,
                                        fontSize,
                                        start: startRadius,
                                        color,
                                        fontFamily: 'SpiralTextFont'
                                    });
                                };
                                const fontPath = changes.data.fontPath.value;
                                if(changes.data.fontPath.prevValue != changes.data.fontPath.value) {
                                    core.utils.loadFont('SpiralTextFont',
                                        fontPath.slice(0, 5).includes('http') ? fontPath : path.join(store.assetRoot, fontPath),
                                        () => {
                                            const nodeData = findNode(node._id);
                                            nodeData.data.version.value = uuidv4();
                                        });
                                }
                                renderLyrics();
                                konvaImageObj.setAttrs(attributes);
                            }
                        );
                    }
                    else if(layerData.type == 'song-mask') {
                        const node = actions.createDocumentNode({
                            title: `Song mask`,
                            type: 'song-mask',
                            schema: schemas.songMask
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        songMaskNodeID = node._id;
                        setCommonData(node);
                        store.document.children.push(node);
                    }
                    else {
                        const node = actions.createDocumentNode({
                            title: `? - ${layerData.type}`,
                            type: 'unknown',
                        });
                        nodeConfigMap[layerIdx] = node._id;
                        setCommonData(node);
                        store.document.children.push(node);
                    }
                }
                events.on('canvasObjectUnselected', () => {
                    actions.setSelectedNode(null);
                });
                (() => {
                    let lastSelectedNodeID = null;
                    setInterval(() => {
                        if(store.selectedNodeID != lastSelectedNodeID) {
                            lastSelectedNodeID = store.selectedNodeID;
                            events.emit('requestCanvasUnselect');
                            localEvents.emit('selectedNodeChange');
                        }
                    }, 300);
                })();
            })();
        };
        const handleDocSave = () => {
            try {
                const backupDir = path.join(store.assetRoot, '__data-backups');
                if (!fs.existsSync(backupDir)){
                    fs.mkdirSync(backupDir, { recursive: true });
                }
                const dataJSONpath = path.join(store.assetRoot, 'data.json');
                const dataJSONraw = fs.readFileSync(dataJSONpath).toString();
                fs.writeFileSync(path.join(backupDir, `data-${(+new Date)}.json`), dataJSONraw);
                const dataJSONbefore = JSON.parse(dataJSONraw);
                const dataJSONafter = {
                    document: dataJSONbefore.document,
                    children: []
                };
                for(const [layerIdx, rawLayer] of dataJSONbefore.children.entries()) {
                    const layerData = {
                        ...parseLayerType(rawLayer.type)
                    };
                    if(layerData.type == 'text') {
                        const nodeData = findNode(nodeConfigMap[layerIdx]);
                        const finalMeta = {
                            ...layerData.meta
                        };
                        if(nodeData.data.module) {
                            finalMeta.module = nodeData.data.module.value;
                        }
                        if(nodeData.data.role) {
                            finalMeta.role = nodeData.data.role.value;
                        }
                        if(nodeData.data.required) {
                            finalMeta.required = nodeData.data.required.value;
                        }
                        if(nodeData.data.label) {
                            finalMeta.label = nodeData.data.label.value;
                        }
                        if(nodeData.data.shape) {
                            finalMeta.shape = nodeData.data.shape.value
                        }
                        const textColor = hexToRGB(nodeData.data.color.value ?? '#000000ff');
                        dataJSONafter.children.push({
                            left: +nodeData.commonData.left.value,
                            top: +nodeData.commonData.top.value,
                            width: +nodeData.commonData.width.value,
                            height: +nodeData.commonData.height.value,
                            rotate: +nodeData.commonData.rotation.value,
                            opacity: +nodeData.commonData.opacity.value,
                            keepRatio: nodeData.commonData.keepRatio.value ?? true,
                            blendMode: 'normal',
                            type: layerData.type + '[' + Object.entries(finalMeta).map(([k, v]) => `${k}:${v}`).join(',') + ']',
                            text: {
                                value: nodeData.data.defaultText?.value ?? nodeData.data.lyrics?.value ?? '',
                                font: {
                                    ...(nodeData.data.lineHeight ? { lineHeight: nodeData.data.lineHeight.value } : {}),
                                    name: nodeData.data.fontName?.value,
                                    sizes: [ +nodeData.data.fontSize?.value ],
                                    colors: [ [textColor.r, textColor.g, textColor.b, textColor.a] ],
                                    alignment: [ nodeData.data.hAlign?.value ],
                                    path: nodeData.data.fontPath.value
                                },
                                box: {
                                    width: +nodeData.commonData.width.value,
                                    height: +nodeData.commonData.height.value
                                },
                            }
                        });
                    }
                    else if(layerData.type == 'custom' && layerData.meta.module == 'spiral') {
                        const nodeData = findNode(nodeConfigMap[layerIdx]);
                        const finalMeta = {
                            ...layerData.meta
                        };
                        const textColor = hexToRGB(nodeData.data.color.value ?? '#000000ff');
                        dataJSONafter.children.push({
                            left: +nodeData.commonData.left.value,
                            top: +nodeData.commonData.top.value,
                            width: +nodeData.commonData.width.value,
                            height: +nodeData.commonData.height.value,
                            rotate: +nodeData.commonData.rotation.value,
                            opacity: +nodeData.commonData.opacity.value,
                            keepRatio: nodeData.commonData.keepRatio.value ?? true,
                            blendMode: 'normal',
                            type: 'custom[module:spiral]',
                            spiral: {
                                startRadius: +nodeData.data.startRadius.value,
                                text: nodeData.data.text.value,
                                letterSpacing: 2,
                                fontFamily: 'SpiralTextFont',
                                fontSize: +nodeData.data.fontSize.value,
                                fontPath: nodeData.data.fontPath.value,
                                color: nodeData.data.color.value,
                                characterLimit: +nodeData.data.characterLimit.value
                            }
                        });
                    }
                    else {
                        const nodeData = findNode(nodeConfigMap[layerIdx]);
                        dataJSONafter.children.push({
                            ...rawLayer,
                            left: +nodeData.commonData.left.value,
                            top: +nodeData.commonData.top.value,
                            width: +nodeData.commonData.width.value,
                            height: +nodeData.commonData.height.value,
                            rotate: +nodeData.commonData.rotation.value,
                            opacity: +nodeData.commonData.opacity.value,
                            keepRatio: nodeData.commonData.keepRatio.value ?? true,
                            blendMode: 'normal',
                        });
                    }
                }
                fs.writeFileSync(dataJSONpath, JSON.stringify(dataJSONafter, null, 4));
                const dialogID = actions.openDialog({
                    title: '',
                    body: `
                        <div class="h-40 w-full flex justify-center items-center">
                            <span class="text-xl">
                                Saved 👌
                            </span>
                        </div>
                    `
                });
                setTimeout(() => {
                    actions.closeDialog(dialogID);
                }, 1000);
            }
            catch(e) {
                actions.openDialog({
                    title: 'Save error',
                    body: `
                        <div class="">
                            <span class="">
                                ${e} 😱
                            </span>
                        </div>
                    `
                });
            }
        };
        events.on('assetRootSet', () => {
            events.remove('assetReady', handleAssetReady);
            events.remove('requestDocSave', handleDocSave);
            let isDocValid = true;
            try {
                const dataJSONpath = path.join(store.assetRoot, 'data.json');
                if(fs.existsSync(dataJSONpath)) {
                    const dataJSON = JSON.parse(fs.readFileSync(dataJSONpath));
                    if(!dataJSON.document || !dataJSON.children || dataJSON.doctype) {
                        isDocValid = false;
                    }
                }
            }
            catch(e) {
                isDocValid = false;
            }
            if(isDocValid) {
                events.one('assetReady', () => {
                    const intervalID = setInterval(() => {
                        if(this.api.stage) {
                            clearInterval(intervalID);
                            handleAssetReady();
                            events.on('requestDocSave', handleDocSave);
                        }
                    }, 500);
                });
            }
        });
        console.log(getSpiralTextBase64({
            width: 1000,
            height: 1000,
            text: 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ',
            fontSize: 50,
            letterSpacing: 2,
            start: 0,
            color: '#000000'
        }));
    }
};
