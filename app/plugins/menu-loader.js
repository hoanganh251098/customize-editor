module.exports = {
    onShow() {
        const {store, actions, React, html} = this.api;
        actions.openDialog({
            title: 'Hello',
            // body: React.createElement('span', { className: 'italic' }, 'Have a nice day :)')
            body: html`
                <span class="italic">
                    Have a nice day 
                </span>
                😁
            `
        });
    },
    onInit() {
        const {
            $,
            document,
            uuidv4,
            core,
            store,
            actions,
            registerAction,
            events
        } = this.api;
        events.register('assetRootSet');
        events.register('requestDocSave');
        const menuHandlers = {
            fileOpen() {
                if(store.assetRoot) {
                    const dialogID = actions.openDialog({
                        title: 'Message',
                        body: `
                            <span class="italic">
                                Sorry, you have to close the document first
                            </span>
                            😺
                        `
                    });
                    setTimeout(() => {
                        actions.closeDialog(dialogID);
                    }, 3000);
                    return;
                }
                const dialogID = actions.openDialog({
                    title: 'Open folder',
                    body: `
                        <input class="w-full h-6 outline-none mt-2 pl-3 pr-6 bg-gray-100 border focus:border-blue-400"
                            placeholder="Folder location"
                            spellCheck="false"
                            autoComplete="off"
                            autoCapitalize="off"
                            autoCorrect="off"
                        ></input>
                        <div class="absolute inline-block right-2 bottom-4">
                            <button
                                class="open-btn flex justify-center items-center px-2 border active:border-blue-400 bg-gray-100"
                            >
                                Open
                            </button>
                        </div>
                    `
                });
                const jdoc = $(document);
                jdoc.find(`#dialog-${dialogID} input`).focus();
                jdoc.one('click', `#dialog-${dialogID} .open-btn`, () => {
                    const folderLocation = jdoc.find(`#dialog-${dialogID} input`).val();
                    actions.setAssetRoot(folderLocation);
                    actions.closeDialog(dialogID);
                    actions.scanAssets();
                });
                jdoc.find(`#dialog-${dialogID} input`).on('keyup', e => {
                    if(e.code != 'Enter') return;
                    const folderLocation = jdoc.find(`#dialog-${dialogID} input`).val();
                    actions.setAssetRoot(folderLocation);
                    actions.closeDialog(dialogID);
                    actions.scanAssets();
                });
            },
            fileClose() {
                const win = nw.Window.get();
                win.reload();
            },
            fileSave() {
                events.emit('requestDocSave');
            }
        };
        registerAction(() => {
            store.menu.children.push({
                title: 'File',
                children: [
                    { title: 'New' },
                    { title: 'Open', onClick: menuHandlers.fileOpen },
                    { title: 'Save', onClick: menuHandlers.fileSave },
                    { title: 'Exports', children: [ { title: 'Nah' } ] },
                    { title: 'Close', onClick: menuHandlers.fileClose },
                ]
            });
            store.menu.children.push({
                title: 'Edit',
                children: [
                    { title: 'Undo', subtitle: 'Ctrl+Z' },
                    { title: 'Redo', subtitle: 'Ctrl+Shift+Z' },
                ]
            });
        })();
        $(this.api.window).bind('keydown', function(event) {
            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    events.emit('requestDocSave');
                    break;
                case 'o':
                    event.preventDefault();
                    menuHandlers.fileOpen();
                    break;
                case 'w':
                    event.preventDefault();
                    menuHandlers.fileClose();
                    break;
                }
            }
        });
    }
};