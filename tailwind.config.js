const _ = require('lodash');
const flattenColorPalette = require('tailwindcss/lib/util/flattenColorPalette').default;

module.exports = {
    // mode: 'jit',
    // purge: {
    //     enabled: true,
    //     content: [
    //         "./src/**/*.{js,jsx,ts,tsx,vue,html}"
    //     ],
    // },
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {},
    variants: {
        extend: {
            backgroundColor: ["active"],
            borderColor: ["active"]
        },
    },
    plugins: [
        function ({ addUtilities, e, theme, variants }) {
            const colors = flattenColorPalette(theme("borderColor"));
            const utilities = _.flatMap(
                _.omit(colors, "default"),
                (value, modifier) => ({
                    [`.${e(`border-t-${modifier}`)}`]: {
                        borderTopColor: `${value}`,
                    },
                    [`.${e(`border-r-${modifier}`)}`]: {
                        borderRightColor: `${value}`,
                    },
                    [`.${e(`border-b-${modifier}`)}`]: {
                        borderBottomColor: `${value}`,
                    },
                    [`.${e(`border-l-${modifier}`)}`]: {
                        borderLeftColor: `${value}`,
                    },
                })
            );
            addUtilities(utilities, variants("borderColor"));
        },
        function ({ addUtilities, e, theme, variants }) {
            const widths = theme("width");
            const utilities = _.flatMap(
                _.omit(widths, "default"),
                (value, modifier) => ({
                    [`.${e(`min-w-${modifier}`)}`]: {
                        minWidth: `${value}`,
                    },
                    [`.${e(`min-h-${modifier}`)}`]: {
                        minHeight: `${value}`,
                    },
                })
            );
            addUtilities(utilities, variants("width"));
        }
    ],
};
